USE [DoGiaDungDataBase]
GO
/****** Object:  Table [dbo].[carts]    Script Date: 5/28/2024 11:55:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[carts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[product_id] [int] NULL,
	[user_id] [int] NULL,
	[price] [decimal](18, 0) NULL,
	[quantity] [int] NULL,
	[size] [int] NULL,
 CONSTRAINT [PK_carts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[categories]    Script Date: 5/28/2024 11:55:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[categories](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](250) NULL,
	[slug] [varchar](200) NULL,
	[status] [int] NULL,
	[created_at] [datetime2](7) NOT NULL,
	[updated_at] [datetime2](7) NULL,
	[icon] [varchar](max) NULL,
	[isdetele] [int] NULL,
 CONSTRAINT [PK_categories] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[contacts]    Script Date: 5/28/2024 11:55:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contacts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[full_name] [nvarchar](250) NULL,
	[phone_number] [varchar](11) NULL,
	[note] [nvarchar](max) NULL,
	[status] [int] NULL,
	[created_at] [datetime2](7) NULL,
 CONSTRAINT [PK_contacts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[images]    Script Date: 5/28/2024 11:55:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[images](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](200) NULL,
	[alt] [nvarchar](400) NULL,
	[status] [int] NULL,
	[product_id] [int] NULL,
 CONSTRAINT [PK_images] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[order_details]    Script Date: 5/28/2024 11:55:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order_details](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order_id] [int] NULL,
	[product_id] [int] NULL,
	[price] [decimal](18, 0) NULL,
	[quantity] [int] NULL,
 CONSTRAINT [PK_order_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[orders]    Script Date: 5/28/2024 11:55:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[orders](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [nvarchar](250) NULL,
	[user_id] [int] NULL,
	[note] [nvarchar](500) NULL,
	[full_name] [nvarchar](150) NULL,
	[phone_number] [varchar](150) NULL,
	[review] [int] NULL,
	[payment] [int] NOT NULL,
	[status] [int] NULL,
	[total] [int] NULL,
	[fee_ship] [int] NULL,
	[created_at] [datetime2](7) NULL,
 CONSTRAINT [PK_orders] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[posts]    Script Date: 5/28/2024 11:55:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[posts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](250) NULL,
	[slug] [varchar](500) NULL,
	[avatar] [varchar](200) NULL,
	[content] [nvarchar](max) NULL,
	[short_content] [nvarchar](500) NULL,
	[created_at] [datetime2](7) NULL,
	[updated_at] [datetime2](7) NULL,
	[status] [int] NULL,
	[author] [int] NULL,
 CONSTRAINT [PK_posts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductCart]    Script Date: 5/28/2024 11:55:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductCart](
	[Id] [int] NOT NULL,
	[ProductId] [int] NULL,
	[CartId] [int] NULL,
 CONSTRAINT [PK_ProductCart] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[products]    Script Date: 5/28/2024 11:55:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[products](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](250) NULL,
	[category_id] [int] NULL,
	[price_sell] [decimal](18, 0) NULL,
	[description] [nvarchar](max) NULL,
	[status] [int] NULL,
	[price_reduced] [decimal](18, 0) NULL,
	[horizontal] [int] NULL,
	[trademark] [nvarchar](500) NULL,
	[price_import] [decimal](18, 0) NULL,
	[trademarkId] [int] NULL,
	[ishidden] [int] NULL,
	[sizes] [varchar](max) NULL,
	[images] [varchar](max) NULL,
	[number_import] [int] NULL,
	[cover_type] [int] NULL,
	[shortdescription] [nvarchar](max) NULL,
	[total_sell] [int] NULL,
 CONSTRAINT [PK_products] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[reviews]    Script Date: 5/28/2024 11:55:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reviews](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[product_id] [int] NULL,
	[created_at] [datetime2](7) NULL,
	[comment] [nvarchar](max) NULL,
	[status] [int] NULL,
	[star] [int] NULL,
	[fullname] [nvarchar](max) NULL,
 CONSTRAINT [PK_reviews] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 5/28/2024 11:55:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[full_name] [nvarchar](250) NULL,
	[phone_number] [varchar](11) NULL,
	[email] [varchar](50) NULL,
	[avatar] [varchar](500) NULL,
	[code] [varchar](10) NULL,
	[address] [nvarchar](max) NULL,
	[role] [int] NULL,
	[password] [varchar](32) NULL,
	[status] [int] NULL,
	[register_date] [datetime2](7) NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[carts] ON 

INSERT [dbo].[carts] ([id], [product_id], [user_id], [price], [quantity], [size]) VALUES (9, 21, 11, CAST(1300000 AS Decimal(18, 0)), 1, NULL)
INSERT [dbo].[carts] ([id], [product_id], [user_id], [price], [quantity], [size]) VALUES (16, 21, 6, CAST(10400000 AS Decimal(18, 0)), 8, NULL)
SET IDENTITY_INSERT [dbo].[carts] OFF
GO
SET IDENTITY_INSERT [dbo].[categories] ON 

INSERT [dbo].[categories] ([id], [name], [slug], [status], [created_at], [updated_at], [icon], [isdetele]) VALUES (1, N'Nhà Bếp', N'nhà b?p', 0, CAST(N'2024-04-03T15:35:44.3684429' AS DateTime2), CAST(N'2024-05-27T13:12:01.6785901' AS DateTime2), N'TIEN-ICH-NHA-BEP.jpg', NULL)
INSERT [dbo].[categories] ([id], [name], [slug], [status], [created_at], [updated_at], [icon], [isdetele]) VALUES (2, N'Phòng Khách', N'phòng khách', 0, CAST(N'2024-04-03T15:39:49.1226254' AS DateTime2), CAST(N'2024-05-27T13:11:58.3464075' AS DateTime2), N'GIA-DUNG-PHONG-KHACH.jpg', NULL)
INSERT [dbo].[categories] ([id], [name], [slug], [status], [created_at], [updated_at], [icon], [isdetele]) VALUES (3, N'Phòng Tắm - Vệ Sinh', N'phòng t?m - v? sinh', 0, CAST(N'2024-04-03T15:41:06.5031362' AS DateTime2), CAST(N'2024-05-27T13:11:52.8879007' AS DateTime2), N'GIA-DUNG-PHONG-TAM.jpg', NULL)
INSERT [dbo].[categories] ([id], [name], [slug], [status], [created_at], [updated_at], [icon], [isdetele]) VALUES (5, N'Nhà Kho', N'nhà kho', 0, CAST(N'2024-05-15T21:55:01.0124423' AS DateTime2), CAST(N'2024-05-27T12:39:29.0396033' AS DateTime2), N'nhakho.jpg', NULL)
INSERT [dbo].[categories] ([id], [name], [slug], [status], [created_at], [updated_at], [icon], [isdetele]) VALUES (8, N'Phòng Ngủ', NULL, 0, CAST(N'2019-05-28T08:59:44.0995075' AS DateTime2), NULL, N'pngu.jpg', NULL)
SET IDENTITY_INSERT [dbo].[categories] OFF
GO
SET IDENTITY_INSERT [dbo].[contacts] ON 

INSERT [dbo].[contacts] ([id], [full_name], [phone_number], [note], [status], [created_at]) VALUES (3, N'Bùi Thị Thơm', N'0978659803', N'chưa có sản phẩm mới
', 1, CAST(N'2024-05-17T21:23:21.0621975' AS DateTime2))
SET IDENTITY_INSERT [dbo].[contacts] OFF
GO
SET IDENTITY_INSERT [dbo].[images] ON 

INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (3, N'1.jpg', NULL, NULL, 3)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (4, N'2.jpg', NULL, NULL, 4)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (5, N'3.jpg', NULL, NULL, 5)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (6, N'4.jpg', NULL, NULL, 6)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (7, N'5.jpg', NULL, NULL, 7)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (8, N'6.jpg', NULL, NULL, 8)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (9, N'tam1.jpg', NULL, NULL, 9)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (10, N'tam2.jpg', NULL, NULL, 10)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (11, N'tam3.jpg', NULL, NULL, 11)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (12, N'tam4.jpg', NULL, NULL, 12)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (13, N'khach2.jpg', NULL, NULL, 13)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (14, N'ban-an-keo-dai-mat-da-thong-minh-510x510.jpg', NULL, NULL, 14)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (15, N'ban-tra-phong-khach-nho-bang-go-dep-cho-chung-cu-kgtn-011bt002.jpg', NULL, NULL, 15)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (16, N'dong-ho-treo-tuong-trang-tri-bang-go-mau-sac-kgtn-011dh001.jpg', NULL, NULL, 16)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (17, N'gia-ke-nha-tam-bang-nhua-3-tang-kgtn-011knt008.jpg', NULL, NULL, 17)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (18, N'nhàkho.png', NULL, NULL, 18)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (20, N'keo.jpg', NULL, NULL, 20)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (21, N'bepnuong.jpg', NULL, NULL, 21)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (28, N'den.jpg', NULL, NULL, 22)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (29, N'xeng.png', NULL, NULL, 19)
INSERT [dbo].[images] ([id], [name], [alt], [status], [product_id]) VALUES (30, N'denngu.jpg', NULL, NULL, 25)
SET IDENTITY_INSERT [dbo].[images] OFF
GO
SET IDENTITY_INSERT [dbo].[order_details] ON 

INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (7, 16, 7, CAST(890000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (8, 17, 9, CAST(170000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (9, 17, 5, CAST(790000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (10, 18, 6, CAST(245000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (11, 19, 13, CAST(3990000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (12, 20, 5, CAST(790000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (13, 21, 11, CAST(230000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (14, 22, 8, CAST(2880000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (15, 23, 14, CAST(4550000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (16, 24, 7, CAST(890000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (17, 25, 10, CAST(170000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (18, 26, 9, CAST(170000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (19, 27, 13, CAST(3990000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (20, 28, 3, CAST(110000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (22, 30, 7, CAST(890000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (23, 31, 15, CAST(3400000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (24, 31, 7, CAST(890000 AS Decimal(18, 0)), 2)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (25, 32, 15, CAST(3400000 AS Decimal(18, 0)), 2)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (26, 33, 7, CAST(890000 AS Decimal(18, 0)), 2)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (27, 34, 5, CAST(790000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (28, 34, 7, CAST(890000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (29, 34, 14, CAST(4550000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (30, 35, 5, CAST(790000 AS Decimal(18, 0)), 2)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (31, 36, 5, CAST(790000 AS Decimal(18, 0)), 2)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (32, 37, 13, CAST(3990000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (33, 38, 13, CAST(3990000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (34, 38, 5, CAST(790000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (35, 39, 16, CAST(1150000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (36, 40, 3, CAST(110000 AS Decimal(18, 0)), 2)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (37, 40, 6, CAST(245000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (38, 41, 5, CAST(790000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (39, 42, 6, CAST(245000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (40, 43, 20, CAST(180000 AS Decimal(18, 0)), 2)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (41, 44, 13, CAST(3990000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (42, 45, 5, CAST(790000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (43, 46, 10, CAST(170000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (44, 47, 7, CAST(890000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (45, 48, 21, CAST(1300000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (46, 49, 16, CAST(1150000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (47, 50, 4, CAST(270000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (48, 51, 3, CAST(110000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (49, 51, 13, CAST(3990000 AS Decimal(18, 0)), 2)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (50, 52, 18, CAST(200000 AS Decimal(18, 0)), 11)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (51, 53, 6, CAST(245000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (52, 53, 5, CAST(790000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (53, 54, 15, CAST(3400000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (55, 55, 5, CAST(790000 AS Decimal(18, 0)), 2)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (56, 56, 7, CAST(890000 AS Decimal(18, 0)), 3)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (59, 59, 6, CAST(245000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (60, 59, 16, CAST(1150000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (61, 60, 7, CAST(890000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (62, 60, 16, CAST(1150000 AS Decimal(18, 0)), 3)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (63, 61, 6, CAST(245000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (64, 62, 6, CAST(245000 AS Decimal(18, 0)), 1)
INSERT [dbo].[order_details] ([id], [order_id], [product_id], [price], [quantity]) VALUES (65, 62, 5, CAST(790000 AS Decimal(18, 0)), 1)
SET IDENTITY_INSERT [dbo].[order_details] OFF
GO
SET IDENTITY_INSERT [dbo].[orders] ON 

INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (16, N'MDH00316', 3, NULL, N'Nguyễn Thị Hương', N'0345801876', NULL, 1, 1, 890000, NULL, CAST(N'2024-02-10T15:55:16.4881561' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (17, N'MDH00356', 3, NULL, N'Nguyễn Thị Hương', N'0345801876', NULL, 0, 1, 960000, NULL, CAST(N'2024-03-20T15:55:56.0627423' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (18, N'MDH0041', 4, NULL, N'Nguyễn Văn Hùng', N'0345801872', NULL, 0, 1, 245000, NULL, CAST(N'2024-03-20T16:39:01.8975657' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (19, N'MDH00441', 4, NULL, N'Nguyễn Văn Hùng', N'0345801872', NULL, 0, 0, 3990000, NULL, CAST(N'2024-03-22T16:39:41.3093148' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (20, N'MDH00454', 4, NULL, N'Nguyễn Văn Hùng', N'0345801872', NULL, 0, 2, 790000, NULL, CAST(N'2024-03-22T16:39:54.3444844' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (21, N'MDH0048', 4, NULL, N'Nguyễn Văn Hùng', N'0345801872', NULL, 1, 1, 230000, NULL, CAST(N'2024-03-22T16:40:08.1033433' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (22, N'MDH00431', 4, NULL, N'Nguyễn Văn Hùng', N'0345801872', NULL, 1, 0, 2880000, NULL, CAST(N'2024-03-22T16:40:31.2608994' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (23, N'MDH00416', 4, NULL, N'Nguyễn Văn Hùng', N'0345801872', NULL, 1, 1, 4550000, NULL, CAST(N'2024-03-23T16:42:16.2622829' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (24, N'MDH00354', 3, NULL, N'Nguyễn Thị Hương', N'0345801876', NULL, 0, 1, 890000, NULL, CAST(N'2024-03-23T16:43:54.1549756' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (25, N'MDH0039', 3, NULL, N'Nguyễn Thị Hương', N'0345801876', NULL, 1, 1, 170000, NULL, CAST(N'2024-03-23T16:44:09.9810040' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (26, N'MDH00329', 3, NULL, N'Nguyễn Thị Hương', N'0345801876', NULL, 1, 0, 170000, NULL, CAST(N'2024-04-22T16:44:29.5235661' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (27, N'MDH00350', 3, NULL, N'Nguyễn Thị Hương', N'0345801876', NULL, 1, 1, 3990000, NULL, CAST(N'2024-04-22T16:44:50.8901082' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (28, N'MDH0034', 3, NULL, N'Nguyễn Thị Hương', N'0345801876', NULL, 1, 1, 110000, NULL, CAST(N'2024-04-22T16:45:04.3056822' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (30, N'MDH00628', 6, NULL, N'Nguyễn Thị Hoài Thu', N'0978659803', NULL, 0, 1, 890000, NULL, CAST(N'2024-05-08T23:38:28.1215053' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (31, N'MDH0065', 6, NULL, N'Nguyễn Thị Hoài Thu', N'0978659803', NULL, 1, 1, 5180000, NULL, CAST(N'2024-05-09T22:50:05.1931486' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (32, N'MDH00611', 6, NULL, N'Nguyễn Thị Hoài Thu', N'0978659803', NULL, 0, 2, 6800000, NULL, CAST(N'2024-05-09T22:57:11.9991438' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (33, N'MDH0062', 6, NULL, N'Nguyễn Thị Hoài Thu', N'0978659803', NULL, 1, 2, 1780000, NULL, CAST(N'2024-05-10T01:47:02.8932151' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (34, N'MDH00637', 6, NULL, N'Nguyễn Thị Hoài Thu', N'0978659803', NULL, 0, 2, 6230000, NULL, CAST(N'2024-05-10T21:45:37.5394274' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (35, N'MDH00410', 4, NULL, N'Nguyễn Văn Hùng', N'0345801872', NULL, 0, 0, 1580000, NULL, CAST(N'2024-05-15T21:18:10.4778592' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (36, N'MDH00415', 4, NULL, N'Nguyễn Văn Hùng', N'0345801872', NULL, 0, 0, 1580000, NULL, CAST(N'2024-05-15T21:22:15.1314101' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (37, N'MDH0067', 6, N'mã thành, yên thành, nghệ an', N'Nguyễn Thị Hoài Thu', N'0978659803', NULL, 0, 1, 3990000, NULL, CAST(N'2024-05-17T15:09:07.1743111' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (38, N'MDH00616', 6, N'Mã thành, yên thành, nghệ an', N'Nguyễn Thị Hoài Thu', N'0978659803', NULL, 1, 2, 4780000, NULL, CAST(N'2024-05-17T15:12:16.6093396' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (39, N'MDH00659', 6, N'Xuân phương, Nam từ Liêm, Hà Nội', N'Nguyễn Thị Hoài Thu', N'0978659803', NULL, 1, 2, 1150000, NULL, CAST(N'2024-05-17T15:19:59.4726920' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (40, N'MDH00616', 6, N'f
', N'Nguyễn Thị Hoài Thu', N'0978659803', NULL, 1, 2, 465000, NULL, CAST(N'2024-05-19T16:12:16.5381454' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (41, N'MDH00628', 6, N'mỹ đình', N'Nguyễn Thị Hoài Thu', N'0978659803', NULL, 0, 2, 790000, NULL, CAST(N'2024-05-19T21:19:28.3035460' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (42, N'MDH00622', 6, NULL, N'Nguyễn Thị Hoài Thu', N'0978659803', NULL, 0, 2, 245000, NULL, CAST(N'2024-05-19T21:43:22.1628319' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (43, N'MDH00624', 6, N'Nghệ An', N'Bùi Thị Thơm', N'0978659803', NULL, 0, 2, 360000, NULL, CAST(N'2024-05-19T22:04:24.4765985' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (44, N'MDH00611', 6, N'Xuân phương, Hà Nội', N'Bùi Thị Thơm', N'0978659803', NULL, 1, 2, 3990000, NULL, CAST(N'2024-05-19T22:05:11.7790312' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (45, N'MDH00645', 6, N'Bắc Từ Liêm, Hà Nội', N'Bùi Thị Thơm', N'0978659803', NULL, 0, 1, 790000, NULL, CAST(N'2024-05-19T22:05:45.7602488' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (46, N'MDH0059', 5, N'Nghĩa Châu, Nam Định', N'Lê Hồng Thắm', N'0345801213', NULL, 1, 1, 170000, NULL, CAST(N'2024-05-19T22:07:09.6690330' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (47, N'MDH0052', 5, N'Hải Hậu, Nam Định', N'Lê Hồng Thắm', N'0345801213', NULL, 0, 1, 890000, NULL, CAST(N'2024-05-19T22:08:02.8434894' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (48, N'MDH0098', 9, N'Lập Thạch, Vĩnh Phúc', N'Châu Thi Vũ', N'0978659888', NULL, 0, 0, 1300000, NULL, CAST(N'2024-05-19T22:09:08.1403935' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (49, N'MDH00955', 9, N'Sông Lô, Vĩnh Phúc', N'Châu Thi Vũ', N'0978659888', NULL, 0, 2, 1150000, NULL, CAST(N'2024-05-19T22:09:55.5945928' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (50, N'MDH00848', 8, N'Diễn Châu, Nghệ An', N'Trần Văn Nhất', N'0936411789', NULL, 0, 1, 270000, NULL, CAST(N'2024-05-19T22:10:48.1354704' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (51, N'MDH0047', 4, N'Hải Phòng', N'Nguyễn Văn Hùng', N'0345801872', NULL, 1, 0, 8090000, NULL, CAST(N'2024-05-19T22:12:07.1888186' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (52, N'MDH00636', 6, N'Yên Thành, Nghệ An', N'Bùi Thị Thơm', N'0978659803', NULL, 1, 2, 2200000, NULL, CAST(N'2024-05-22T16:11:36.2829231' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (53, N'MDH00622', 6, NULL, N'Bùi Thị Thơm', N'0978659803', NULL, 0, 2, 1035000, NULL, CAST(N'2024-05-23T23:46:22.0534655' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (54, N'MDH00527', 5, NULL, N'Lê Hồng Thắm', N'0345801213', NULL, 0, 2, 3400000, NULL, CAST(N'2024-05-26T16:39:27.9697158' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (55, N'MDH0066', 6, N'Bắc Từ Liêm Hà Nội', N'Bùi Thị Thơm', N'0978659803', NULL, 0, 2, 1680000, NULL, CAST(N'2024-05-26T18:07:06.0104605' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (56, N'MDH00644', 6, N'Nghệ An', N'Bùi Thị Thơm', N'0978659803', NULL, 1, 1, 2670000, NULL, CAST(N'2024-05-27T00:52:44.2522208' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (57, N'MDH00445', 4, NULL, N'Nguyễn Văn Hùng', N'0345801872', NULL, 0, 2, 66, NULL, CAST(N'2024-05-27T11:09:45.0598476' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (59, N'MDH001119', 11, N'Nghĩa Hưng, Nam Định', N'Nguyễn Thị Nga', N'0345801234', NULL, 0, 1, 1395000, NULL, CAST(N'2024-05-27T13:46:19.8312527' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (60, N'MDH0066', 6, N'Nghệ An', N'Bùi Thị Thơm', N'0978659803', NULL, 0, 2, 4340000, NULL, CAST(N'2024-05-28T00:54:06.3985423' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (61, N'MDH0064', 6, N'Nam Định', N'Bùi Thị Thơm', N'0978659803', NULL, 0, 1, 245000, NULL, CAST(N'2024-05-28T00:55:04.2871492' AS DateTime2))
INSERT [dbo].[orders] ([id], [code], [user_id], [note], [full_name], [phone_number], [review], [payment], [status], [total], [fee_ship], [created_at]) VALUES (62, N'MDH00638', 6, N'Nghệ An', N'Bùi Thị Thơm', N'0978659803', NULL, 1, 2, 1035000, NULL, CAST(N'2019-05-28T08:53:38.8660572' AS DateTime2))
SET IDENTITY_INSERT [dbo].[orders] OFF
GO
SET IDENTITY_INSERT [dbo].[posts] ON 

INSERT [dbo].[posts] ([id], [title], [slug], [avatar], [content], [short_content], [created_at], [updated_at], [status], [author]) VALUES (1, N'Top 1 kệ đựng đồ rẻ đẹp giá gốc', N'top-1-k?-d?ng-d?-r?-d?p-giá-g?c', N'1-1.jpg', N'<div class="entry-content single-page">
 
 <p>Kệ đựng đồ&nbsp;là sản phẩm không thể thay thế, hiện nay tại các gia đình, cơ quan, trường học cần lưu trữ và bảo quản đồ dùng. Tuy nhiên, nên lựa chọn mẫu kệ nào để phù hợp với nhu cầu sử dụng và không gian sống?</p>
<p>Cùng tìm hiểu qua bài viết dưới đây nhé!</p>
<h2><strong>1. Những mẫu kệ đựng đồ trong gia đình phổ biến</strong></h2>
<h3><strong>1.1. Giá kệ đựng đồ đa năng ưu chuộng</strong></h3>
<p>Giá kệ đựng đồ năng có nhiều kích thước, số lượng tầng, chất liệu và kiểu dáng khác nhau. Các mẫu kệ bằng nhựa cứng, gỗ được sử dụng nhiều trên thị trường nhờ có giá thành rẻ và tiện dụng.</p>

<p>Kệ đựng đồ đa năng giúp bạn giải quyết triệt để vấn đề sắp xếp đồ dùng, tạo không gian sống gọn gàng, thẩm mỹ. Kệ có thiết kế thông minh, dễ dàng lắp ráp và tháo rời phù hợp với mọi không gian trong gia đình.</p>
<h3><strong>1.2. Giá kệ đựng đồ dùng, sách vở&nbsp;</strong></h3>
<p>Sách vở là đồ dùng không thể thiếu tại hầu hết các gia đình, các nhân hiện nay. Nhiều người có sở thích đọc sách và sưu tầm các sản phẩm sách. Kệ đựng đồ dùng, sách vở trở thành vật dụng được nhiều người lựa chọn để mang lại không gian sống hiện đại, sang trọng. Giá kệ đựng đồ dùng, sách vở giúp hệ thống trưng bày tinh tế và tiết kiệm chi phí.</p>

<p>Giá kệ đựng đồ dùng, sách vở được làm từ nhiều loại chất liệu khác nhau từ nhựa, kính, tôn thép cho đến inox, gỗ…Kệ đựng đồ cũng có nhiều thiết kế với các đặc điểm về trưng bày và thẩm mỹ khác nhau. Tùy thuộc vào sở thích và nhu cầu lưu trữ khách hàng có thể lựa chọn kệ phù hợp với không gian trưng bày hài hòa.</p>
<p>Ngoài việc dùng để mang lại tính thẩm mỹ cho không gian sống giá kệ đựng đồ dùng, sách vở còn giúp bảo dưỡng, lưu trữ sách vở sạch sẽ, việc lau chùi bảo dưỡng vệ sinh dễ dàng.</p>
<p>Giá của bộ kệ đựng đồ dùng, sách vở tùy thuộc và chất liệu, kích thước và thiết kế của kệ, hiện nay trên thị trường bạn có thể mua được các sản phẩm kệ có giá giao động từ 250.000- 700.000 đối với kệ có chất liệu bằng gỗ.</p>
<h3><strong>1.3. Giá kệ đựng đồ nhà bếp&nbsp;</strong></h3>
<ul>
<li><strong>Kệ đựng đồ chén bát:</strong>&nbsp;Kệ có nhiều loại với các thiết kế khác nhau về hình dáng, màu sắc và chất liệu. Tuy nhiên các mẫu kệ đựng đồ này có đặc điểm chung đó là nhỏ gọn tiết kiệm không gian trong gian bếp. Có khả năng lưu trữ hầu hết các dụng cụ như bát, đĩa, dao, đũa, thìa, thớt…</li>
</ul>

<p>Với thiết kế thông minh kệ mang lại khả năng sắp xếp dụng cụ hoàn hảo. Giá thành của kệ giao động từ vài trăm nghìn với các kệ bằng nhựa có kích thước nhỏ sử dụng cho cá nhân. Các mẫu kệ để chén bát được làm bằng inox lại có giá đắt hơn từ 1 triệu- 2,5 triệu.</p>
<ul>
<li><strong>Kệ đựng đồ để gầm bếp</strong>: Kệ để gầm bếp có thiết kế nhỏ và thường được mua dựa theo diện tích của ngăn bếp sau khi trừ diện tích của các ống thoát, đường nước…kệ có thể điều chỉnh được chiều dài và thay đổi các kiểu khác nhau tùy thuộc vào nhu cầu lắp đặt.</li>
</ul>

<p>Với thiết kế nhỏ gọn kệ có thể lắp được ở nhiều không gian nhỏ hẹp, phù hợp khi lắp dưới các bồn rửa bát, bồn rửa mặt… giúp khách hàng tận dụng được không gian nhỏ hẹp nhất để lưu trữ đựng đồ dùng.</p>
<p>Đối với các vị trí dưới các bồn thường có đường ống thoát nước vì vậy&nbsp;kệ được thiết kế mang lại khả năng thay đổi vị trí các lỗ, các tấm khay nhựa, di chuyển linh hoạt để tạo khoảng cách với đường ống.</p>
<h3><strong>1.4. Giá kệ đựng đồ bằng gỗ</strong></h3>

<p>Giá kệ để đồ bằng gỗ được sử dụng rộng rãi tại các gia đình, nhà kho, trường học, cơ quan làm việc. Kệ có khả năng lưu trữ, nâng đỡ, lưu trữ các loại hàng hóa khác nhau. Thông thường các giá bằng gỗ được thiết kế đơn giản nhưng lại mang phong cách hiện đại tạo nên không gian tinh tế, thẩm mỹ cao cho người sử dụng.</p>
<p>Tại các không gian như phòng khách, phòng tiếp tân, trường học…thường lưu trữ các sản phẩm nhỏ thì kệ được làm bằng gỗ công nghiệp và có nhiều kiểu dáng phong phú, hiện đại hơn.</p>
<h3><strong>1.5. Giá kệ sắt để đồ đa năng tốt nhất</strong></h3>

<p>Giá kệ sắt để đồ đa năng có đặc điểm tải trọng nặng, chắc chắn vì sử dụng khung sắt.</p>
<p>Nếu bạn đang muốn tìm kiếm một loại giá kệ để đồ thật chắc chắn chứa được nhiều đồ thì đây là sự lựa chọn hoàn hảo. Loại kệ này được lắp ghép linh hoạt từ các thanh thép v lỗ chắc chắn. Thông thường sẽ có các mâm tầng số lượng mâm từ 2 -&gt; 6 tầng.</p>
<p>Bạn có thể để đồ trong gia đình trên mẫu kệ sắt này ở nhiều nơi như nhà bếp, kho gia đình, để thức ăn gần khu vực chăn nuôi,..</p>
<div>
<h3 class="video-container"><strong>1.6. Kệ đựng đồ chơi cho bé</strong></h3>
</div>
<p>Đối với gia đình có trẻ nhỏ thường sử dụng kệ đồ chơi cho bé để lưu giữ,&nbsp;chứa đồ chơi tránh các mối nguy hiểm cho con trẻ như bị vấp, trượt ngã. Kệ cũng giúp bảo quản, sắp xếp đồ chơi gọn gàng cho không gian thông thoáng khi không cần sử dụng.</p>

<p>Cha mẹ dễ dàng tìm kiếm đồ chơi nếu trẻ cần một món đồ để nghịch trong giờ ăn cơm hay tắm rửa…Kệ được thiết kế bằng các bộ phận lắp ghép giúp di chuyển tháo ráp khi cần. Cha mẹ cũng có thể dạy cho trẻ tính tự lập khi sắp xếp, cất gọn đồ đạc.</p>
<h3><strong>1.7. Kệ đựng đồ trang điểm</strong></h3>

<p>Nhu cầu sử dụng mỹ phẩm hiện nay của chị em phụ nữ ngày càng tăng cao việc sử dụng kệ để đồ trang điểm trở thành đồ dùng không thể thiếu. Kệ có nhiều loại tùy thuộc vào diện tích trưng bày, nhu cầu&nbsp;lưu trữ. Kệ có được làm từ nhiều chất liệu như nhựa, gỗ, inox…Bạn có thể lựa chọn kệ theo sở thích và không gian cần lưu trữ mỹ phẩm.</p>
<h3><strong>1.8. Giá đựng đồ trong nhà tắm&nbsp;</strong></h3>

<p>– Giá kệ dán tường:&nbsp; Kệ có khả năng sắp xếp gọn gàng mỹ phẩm và đồ dùng trong nhà tắm. Giúp bạn có thể đặt được những vật dụng cơ bản như xà bông, chai lọ, mỹ phẩm, dễ dàng lấy sử dụng khi cần mang lại không gian nhà tắm hiện đại, tiết kiệm hơn.</p>
<p>Thông thường kệ để trong nhà tắm có cấu tạo thông minh dễ dàng tháo lắp, di chuyển khi sử dụng phù hợp với mọi không gian nhà tắm.</p>
<h3><strong>1.9. Kệ đựng đồ phòng ngủ</strong></h3>
<p>Không gian phòng ngủ ngoài các nội thất cơ bản như giường, tủ quần áo, bàn ghế thì kệ để đồ phòng ngủ cũng là thiết bị không thể thiếu&nbsp;để lưu trữ, trang trí không gian phòng ngủ nổi bật và gọn gàng hơn.</p>

<p>Kệ có nhiều mẫu khác nhau như kệ treo tường, kệ để đứng, kệ góc… Tùy thuộc vào sở thích và nhu cầu sử dụng lưu trữ đồ dùng bạn có thể lựa chọn kệ để tạo ra màu sắc sự đồng điệu hài hòa cho tổng thể căn phòng.</p>
 
 
 </div>', N'Kệ đựng đồ là sản phẩm không thể thay thế, hiện nay tại các gia đình, cơ quan, trường học cần lưu trữ và bảo quản đồ dùng. Tuy nhiên, nên lựa chọn mẫu kệ nào để phù hợp với nhu cầu sử dụng và không gian sống?', CAST(N'2024-04-05T15:45:26.9720891' AS DateTime2), NULL, 0, 1)
INSERT [dbo].[posts] ([id], [title], [slug], [avatar], [content], [short_content], [created_at], [updated_at], [status], [author]) VALUES (2, N'Kệ đựng chén đĩa cao cấp giá ưu đãi', N'k?-d?ng-chén-dia-cao-c?p-giá-uu-dãi', N'tin2.png', N'<div class="entry-content single-page">
 
 <p><strong>Kệ đựng chén đĩa</strong>&nbsp;là một trong những vật dụng quen thuộc, hỗ trợ cho gian bếp nhà bạn thêm gọn gàng, khoa học hơn. Không chỉ thế, mà chất liệu tạo nên kệ chén cũng ảnh hưởng trực tiếp đến sức khỏe của cả gia đình bạn.</p>
<p>Do đó việc lựa chọn sử dụng kệ chén loại nào tốt cũng là vấn đề đau đầu của khá nhiều chị em nội trợ hiện nay. Trên thị trường hiện nay, giá đựng chén đĩa rất đa dạng về mẫu mã, chất liệu cũng như giá thành và nhà cung cấp.</p>
<p>Vì thế, nếu bạn muốn tham khảo nhiều mẫu giá để bát đĩa đa năng cho nhà bếp thêm đẹp, tiện ích và tối ưu gian bếp hãy đọc bài viết dưới đây nhé.</p>
<h2><strong>1. Các mẫu kệ đựng chén đĩa giá ưu đãi</strong></h2>
<h3><strong>1.1. Kệ đựng chén đĩa inox 2 tầng</strong></h3>
<p>Kệ đựng chén đĩa theo phong cách thời thượng và hiện đại. Thiết kế dung tích rộng, đựng được nhiều bát đĩa cốc chén, thìa dĩa… Kết cấu ổn định, không bị rung lắc, dễ dàng lắp đặt, có tính thẩm mỹ cao. Kệ có thiết kế nhỏ gọn, tích hợp nhiều chức năng, dễ dàng bố trí, sắp đặt.</p>

<p>Thiết kế lớp vành chắn inox chống ăn mòn, kẻ vạch, chống vân tay… Rãnh tự động thoát nước thông minh, hệ thống tích nước hợp lý, giúp cho chén đĩa, đũa thìa luôn khô ráo, sạch sẽ. Chất liệu inox sáng đẹp, bền và không gỉ khi tiếp xúc nước lâu ngày. Là chất liệu kệ đựng chén đĩa rất được khách hàng ưa chuộng.</p>

<ul>
<li><strong>Thông tin sản phẩm</strong></li>
</ul>
<p>Màu sắc: Đen xám</p>
<p>Thương hiệu: HMJ</p>
<p>Nguồn gốc: Trung Quốc</p>
<p>Kích thước: 46.5×34.8x14cm</p>
<p>Kiểu dáng: Hình chữ nhật</p>
<p>Chất liệu: Thép không gỉ + Kim loại + Nhựa PP cao cấp</p>
<p>Trọng lượng: 5kg</p>

<p>Hiện Gia Dụng Sài Gòn đang triển khai chương trình ưu đãi khuyến mại với sản phẩm kệ đựng chén đĩa 2 tầng.</p>
<h3><strong>1.2. Mẫu kệ đựng chén đĩa đa năng dành cho tủ bếp dưới</strong></h3>
<p>Đây là loại phụ kiện tủ bếp thông minh phổ biến trên thị trường khi các gia đình lắp đặt dành cho tủ bếp dưới. Kệ đựng chén đĩa đa năng dành cho tủ bếp dưới có thể đựng được rất nhiều đồ dùng như bát đĩa, xoong nồi, dao thớt… có các ngăn chia thông minh, tiện lợi. Giúp bếp luôn gọn gàng, đẹp mắt, tận dụng tối đa không gian bếp.</p>
<p>Mẫu sản phẩm giá bát đa năng này hay còn gọi là&nbsp;kệ đựng chén đĩa xoong nồi đa năng, trang bị ray giảm chấn 2 bên có thể kéo đẩy êm ái, nhẹ nhàng. Sản phẩm phổ biến với việc sử dụng 2 loại inox là inox 201 và inox 304.</p>

<h3><strong>1.3. Mẫu kệ đựng chén đĩa đa năng cao cấp&nbsp;</strong></h3>
<p>Với thiết kế&nbsp;kệ đựng chén đĩa đa năng dành cho tủ bếp trên, cơ cấu chuyển động di động và linh hoạt của các mẫu giá bát nâng hạ chắc chắn là một trong những gợi ý số một. Giá bát nâng hạ&nbsp;được xem là mẫu giá để bát đĩa đa năng, thông minh với thiết kế lắp đặt dành cho tủ bếp trên.</p>
<p>Kệ đựng chén đĩa đa năng thiết kế thông minh với cơ cấu nâng lên – hạ xuống tiện lợi theo mọi ý muốn của người dùng. Với các thiết kế tủ bếp cao hơn so với tầm với của người dùng giá bát di dộng nâng hạ đảm bảo sử dụng dễ dàng hơn, đựng được số lượng lớn bát đĩa đồng thời tăng thêm thẩm mỹ và sự sang trọng cho nhà bếp hiện đại.</p>

<h3><strong>1.4. Kệ đựng chén đĩa độc lập có sấy</strong></h3>
<p>Với thiết kế sang trọng và nhiều tính năng vượt&nbsp;&nbsp;Kệ đựng bát đĩa để bàn&nbsp;là một giải pháp tối ưu mang lại cho không gian bếp nhà bạn luôn được gọn gàng, ngăn nắp.&nbsp;Đây là sản phẩm chất lượng nhất dành cho mọi gia đình bởi không chỉ sự sang trọng từ ngọai hình mà nó đem lại hiệu quả cũng rất cao khi có tác dụng sấy khô bát đũa và khủ sạch vi khuẩn.</p>

<p>Đối với loại giá đựng chén đĩa độc lập này còn có thêm các loại có nắp đậy, đảm bảo vệ sinh tuyệt đối cho người tiêu dùng thêm sự lựa chọn.</p>
<ul>
<li><strong>Địa chỉ mua online chính hãng</strong></li>
</ul>

<p>Được thành lập vào năm 2018, hệ thống cửa hàng&nbsp;<strong>Gia Dụng Sài Gòn</strong>&nbsp;thuộc quyền quản lý của Công ty TNHH Thương Mại TNL, chuyên cung cấp các sản phẩm giá kệ đa năng, đồ gia dụng thông minh, tiện ích cho phòng bếp, phòng khách, phòng tắm,… của gia đình bạn. Giúp cho không gian sống của bạn trở nên gọn gàng, ngăn nắp hơn với những sản phẩm của chúng tôi.</p>
<ul>
<li><strong>Cam kết chính hãng</strong></li>
</ul>
<p>Các sản phẩm tại Gia Dụng Sài Gòn luôn được cam kết là hàng chính hãng, có nguồn gốc xuất xứ rõ ràng và luôn được kiểm tra kỹ càng trước khi bày bán tại cửa hàng.</p>
<ul>
<li><strong>Chính sách bảo hành, vận chuyển và tích điểm hấp dẫn</strong></li>
</ul>
<p>Với chính sách bảo hành, chính sách vận chuyển và chính sách tích điểm hấp dẫn, chúng tôi luôn hy vọng đem lại những trải nghiệm tốt nhất cho khách hàng.</p>
<ul>
<li><strong>Hệ thống cửa hàng online và offline rộng khắp</strong></li>
</ul>
<p>Ngoài hệ thống cửa hàng tại TP Hồ Chí Minh chúng tôi còn phát triển hệ thống bán hàng trực tuyến tại địa chỉ website: https://www.giadungsaigon.com.vn/ và Fanpage: https://www.facebook.com/GiaDungSaiGonTNL&nbsp;giúp khách hàng dễ dàng tra cứu thông tin và mua sắm mỗi ngày.</p>
<ul>
<li><strong>Chăm sóc khách hàng chuyên nghiệp</strong></li>
</ul>
<p>Đội ngũ chăm sóc khách hàng tại Gia dụng Sài Gòn được đào tạo bài bản và chuyên nghiệp. Ngoài chất lượng sản phẩm, dịch vụ thì yếu tố “con người” luôn được chúng tôi đặt lên hàng đầu. Mỗi thành viên tại Gia Dụng Sài Gòn luôn lắng nghe, thấu hiểu và tận tâm với khách hàng. Chúng tôi rất mong được phục vụ ngày càng nhiều hơn nữa những khách hàng mua hàng online đến từ các tỉnh thành trên toàn quốc.</p>

 
 
 </div>', N'Kệ đựng chén đĩa là một trong những vật dụng quen thuộc, hỗ trợ cho gian bếp nhà bạn thêm gọn gàng, khoa học hơn. Không chỉ thế, mà chất liệu tạo nên kệ chén cũng ảnh hưởng trực tiếp đến sức khỏe của cả gia đình bạn.', CAST(N'2024-04-05T15:47:43.6507349' AS DateTime2), NULL, 0, 1)
INSERT [dbo].[posts] ([id], [title], [slug], [avatar], [content], [short_content], [created_at], [updated_at], [status], [author]) VALUES (3, N'Giá đựng đồ thông minh chỉ có tại gia dụng sài gòn', N'giá-d?ng-d?-thông-minh-ch?-có-t?i-gia-d?ng-sài-gòn', N'tun3.png', N'<div class="entry-content single-page">
 
 <p><strong>Giá đựng đồ thông minh</strong>&nbsp;sẽ giúp bạn một cảm giác thật sảng khoái khi bạn đang sống trong không gian có diện tích nhỏ, việc của bạn không phải chỉ là chọn những nội thất để cất trữ đồ đạc gọn gàng, mà còn cần chọn những đồ dùng mang đến thật nhiều tiện ích và vẻ đẹp độc đáo cho căn nhà của bạn.</p>

<p>Giá đựng đồ thông minh giúp tối đa hoá không gian hẹp, và mang đến thật nhiều khoảng lưu trữ đồ luôn là điều kiện đầu tiên để lựa chọn giáđựng đồ. Không những thế, góc nhỏ trong bất kỳ khu vực chức năng nào đều đẹp ấn tượng nhờ cách thiết kế vô cùng độc đáo của những chiếc giá đựng đồ thông minh này.</p>
<h2 id="ti-sao-gia-inh-nen-la-chn-k-gp-gn-thong-minh">Tại sao gia đình nên lựa chọn gía để đồ thông minh</h2>
<p>Không phải tự nhiên những mẫu kệ gấp gọn thông minh ngày nay lại trở thành một phụ kiện thiết yếu trong mỗi gia đình. Những lý do sau đây sẽ giải đáp cho bạn tại sao nên lựa chọn một loại kệ gấp cho gia đình.</p>
<h4 id="thit-k-gp-gn-n-gin-nhng-khong-lam-mt-i-v-p-trang-nha-thanh-lch"><em>Thiết kế gấp gọn đơn giản nhưng không làm mất đi vẻ đẹp trang nhã, thanh lịch</em></h4>
<p>Mẫu giá gấp gọn được thiết kế cực kỳ tiện lợi, kết cấu thông minh là một trong những điểm nổi bật để khách hàng có thể ưu ái loại giá này hơn. Bên cạnh đó, hầu hết các loại kệ gấp gọn cũng được chăm chút bề ngoài rất nhiều, mang màu sắc, hình dáng đơn giản nhưng lại không quá cầu kỳ. Ngoài việc tiện lợi giá gấp gọn thông minh ngày nay cũng đảm nhận tốt vai trò trang trí không gian nhà ở.</p>
<h4 id="kt-cu-k-gp-gn-chinh-hang-cc-k-chc-chn"><em>Kết cấu gấp gọn chính hãng cực kỳ chắc chắn</em></h4>
<p>Nếu bạn đã từng được sử dụng một mẫu kệ gấp gọn chính hãng thì chắc chắn bạn sẽ yêu thích loại giá này nhiều hơn bởi sự chắc chắn, cứng cáp của nó. Hầu như các mẫu giá gấp gọn đều được sản xuất từ các chất liệu cao cấp như PP, carbot chống gỉ được phủ một lớp sơn tĩnh điện..v..v.<br>
Vì thế có thể bảo đảm tối đa được sự chắc chắn trong quá trình sử dụng, mực độ bảo hành cũng nằm trong khoảng thời gian dài hơn cả.</p>
<h4 id="thit-k-chc-nng-k-tin-li"><em>Thiết kế chức năng &nbsp;tiện lợi</em></h4>
<p>Các loại giá gấp gọn luôn nổi bật với ưu điểm tiện nghi cho không gian, chỉ cần sở hữu một sản phẩm giá gấp gọn không gian bếp của bạn đã có nhiều hơn, đồ đạc cũng có nhiều diện tích hơn để sắp xếp.</p>
<p>Ngoài ra, hầu hết các mẫu kệ gấp gọn đều được trang bị tốt khả năng di chuyển linh hoạt, gấp gọn thông minh và có thể xoay 360 độ..v..v.</p>
<p>trong bài viết dưới đây, chúng tôi sẽ giới thiệu đến bạn đọc các mẫu giá đựng đồ thông minh, giúp cho căn nhà rở lên sang trọng , lịch sự và đầy hấp dẫn.</p>
<h2 class="product-title product_title entry-title">Giá đựng đồ vuông thông minh</h2>
<p>giá đựng đồ theo khuôn dáng vuông thông minh &nbsp;được sử dụng linh hoạt từ phòng khách , phòng bếp, phòng ngủ đến phòng tắm đều có thể<br>
sử dụng linh hoạt để đựng đồ dùng cá nhân đến bảo quản thực phẩm.</p>

<p>Phòng tắm gia đình có thể đựng đồ cá nhân của từng người theo từng tầng hoặc để riêng từng đồ vật các tầng khác nhau<br>
. Chưa bao giờ việc bố trí phòng tắm nhà bạn trở lên đơn giản và sang trọng như bây giờ</p>
<p>Nhà bếp là sự lựa chọn hoàn hảo của việc bố trí dụng cụ , gia vị nấu ăn của gia đình. Kệ xoay thông minh WinCi được làm từ thép CacBon<br>
phủ sơn tĩnh điện chống xước, việc bảo quản và vệ sinh rất dễ dàng lên bạn yên tâm không sợ mùi thức ăn, ẩm mốc bám trên kệ .</p>

<h3><strong>Thông số kỹ thuật sản phẩm:</strong></h3>
<ul>
<li>Kích thước:
<ul>
<li>KỆ 3 TẦNG: 14x26x63.5cm</li>
<li>KỆ 4 TẦNG: 14x26x78.5cm</li>
<li>KỆ 5 TẦNG: 14x26x9.5cm</li>
</ul>
</li>
<li>Trọng lượng: 4kg – 7kg</li>
<li>Chất liệu: Thép Carbon, sơn tĩnh điện 3 lớp an toàn</li>
<li>Độ chịu lực: 150kg</li>
<li>Thiết kế: Sang trọng, tinh tế và khoa học</li>
<li>Công dụng: Đựng đa dạng các loại đồ vật trong gia đình</li>
</ul>
<h2>Giá đựng đồ gấp gọn thiết kế mẫu tròn</h2>
<p>Mẫu giá đựng đồ dáng trọn được thiết kế theo phong cách Châu u trang nhã nhưng không làm mất đi vẻ sang trọng vốn có của nó. Được sản xuất bằng chất liệu thép carbon có một lớp sơn tĩnh điện khá dày bởi vậy nó có độ bóng bề cao, không dễ chị xước gây mất thẩm mỹ.</p>

<p>Mẫu giá này chân được gắn thêm bánh xe có thể di chuyển đến mọi không gian khác nhau rất dễ dàng, bánh xe xoay được 360 độ linh hoạt, tự nhiên.<br>
Mỗi tầng được trang bị bằng một lớp lưới khá dày và chắc chắn, bảo đảm khi đồ vật để lên không thể bị rơi vỡ nhưng có thể thấm hút ráo nước ngăn ngừa tạo thành không gian ẩm mốc dễ làm hư hỏng rau củ hay một số vật dụng để trên giá</p>
<p>Khi không sử dụng đến bạn có thể dễ dàng gấp gọn lại, vừa ngăn ngừa tốt bụi bẩn một số côn trùng còn bảo quản đồ vật bên trong rất tốt. Mẫu này có thể đựng rau củ, hoa quả, đồ mỹ phẩm hay một số vật tư sử dụng trong gia đình.</p>
<h2>Giá đựng đồ đa năng cho nhà bếp</h2>
<p>Mẫu kệ chuyên được sử dụng nhiều cho nhà bếp, khác với thiết kế gấp gọn như mẫu ở trên. Loại kệ gấp gọn này thiên về tính cứng cáp và chịu được vật nặng nhiều hơn. Bạn có thể dễ dàng đặt lên các loại nồi cơm điện, lò vi sóng, nồi áp suất hay bất kỳ đồ điện nào có trong nhà bếp. Với sức chịu lực tốt đến 400kg, kệ gỗ này được trang bị ghép 2 tấm/ tầng có thể làm giảm lực ép và phân tán sức nặng tốt hơn, trung bình mỗi tầng có thể chịu được 80kg.</p>

<p>Diện tích mỗi tầng có lòng sâu 34cm, với diện tích siêu rộng này bạn có thể thoải mái để những đồ vật có cỡ lớn. Kê được làm từ chất liệu chống gỉ tốt, là loại thép hợp kim dày dặn sơn thêm 3 lớp sơn tĩnh điện vì vậy có độ bền không gỉ và chống nước hoàn hảo.</p>
<p>Bánh xe có thể xoay 360 độ và chốt cố định vị trí linh hoạt, giúp bạn di chuyển đồ đạc trong nhà bếp nhanh gọn mà không gây tiếng ồn, không tốn sức.<br>
Kết cấu kệ gỗ gấp cực kỳ thoáng khí, không bám bụi, đặc biệt có tính thẩm mỹ cao vì vậy sản phẩm cũng là lựa chọn số 1 cho công cuộc trang trí phòng bếp gọn gàng, tươm tất hơn.</p>
 
 
 </div>', N'Giá đựng đồ thông minh sẽ giúp bạn một cảm giác thật sảng khoái khi bạn đang sống trong không gian có diện tích nhỏ, việc của bạn không phải chỉ là chọn những nội thất để cất trữ đồ đạc gọn gàng, mà còn cần chọn những đồ dùng mang đến thật nhiều tiện ích và vẻ đẹp độc đáo cho căn nhà của bạn.', CAST(N'2024-04-05T15:49:38.8479912' AS DateTime2), NULL, 0, 1)
INSERT [dbo].[posts] ([id], [title], [slug], [avatar], [content], [short_content], [created_at], [updated_at], [status], [author]) VALUES (4, N'Kệ đựng chén đĩa cao cấp giá ưu đãi ship toàn quốc', N'k?-d?ng-chén-dia-cao-c?p-giá-uu-dãi-ship-toàn-qu?c', N'tin4.png', N'<div class="entry-content single-page">
<div>
<p><a href="https://giadungsaigon.com.vn/ke-up-chen-bat-dia-thong-minh/" rel="noopener" target="_blank"><strong>Kệ đựng ch&eacute;n đĩa</strong></a> l&agrave; phụ kiện kh&ocirc;ng thể thiếu trong mỗi kh&ocirc;ng gian <strong>nh&agrave; bếp</strong> của c&aacute;c gia đ&igrave;nh. Đ&acirc;y l&agrave; d&ograve;ng sản phẩm hỗ trợ c&ocirc;ng việc bếp n&uacute;c rất tuyệt vời, gi&uacute;p <strong>sắp xếp</strong> <strong>tủ bếp</strong> của gia đ&igrave;nh bạn trở n&ecirc;n gọn g&agrave;ng v&agrave; tiện nghi hơn.</p>

<p>Với vẻ đẹp s&aacute;ng b&oacute;ng c&ugrave;ng kiểu d&aacute;ng đa dạng chắc chắn d&ograve;ng sản phẩm n&agrave;y &nbsp;sẽ l&agrave;m h&agrave;i l&ograve;ng tất cả mọi kh&aacute;ch h&agrave;ng. Hiện Gia dụng S&agrave;i G&ograve;n c&oacute; 2 d&ograve;ng gi&aacute; b&aacute;t ch&iacute;nh l&agrave; gi&aacute; b&aacute;t cố định v&agrave; gi&aacute; b&aacute;t n&acirc;ng hạ;&nbsp;dưới đ&acirc;y l&agrave; to&agrave;n bộ th&ocirc;ng tin về ưu v&agrave; nhược điểm mỗi d&ograve;ng gi&uacute;p bạn dễ d&agrave;ng h&igrave;nh dung v&agrave; chọn lựa được <strong>mẫu m&atilde;</strong> &ndash; <strong>k&iacute;ch thước lọt l&ograve;ng</strong> ph&ugrave; hợp.</p>

<p><input alt="" src="https://down-vn.img.susercontent.com/file/sg-11134201-23020-sa7ik9gbzmnv74" style="width: 50px; height: 50px; margin-top: 0px; margin-bottom: 0px; float: left;" type="image" />&nbsp;Khuyến m&atilde;i: Khi mua kệ tăng ngay 1 cốc thủy tinh uống nước xuất xứ từ nhật bản&nbsp;</p>

<p>&nbsp;1. Cốc được l&agrave;m từ chất liệu borosilicate cao, c&oacute; thể chịu được nhiệt độ từ -20&deg;C đến 130&deg;C, bền bỉ, tay cầm ho&agrave;n hảo kh&ocirc;ng dễ d&agrave;ng kh&ocirc;ng bị hư hại, bạn c&oacute; thể thưởng thức c&agrave; ph&ecirc;, sữa hoặc c&aacute;c loại nước tr&aacute;i c&acirc;y kh&aacute;c trong thời gian d&agrave;i.</p>

<p>2. Cốc thủy tinh trong suốt c&oacute; thiết kế h&igrave;nh s&oacute;ng tinh tế, cầm tr&ecirc;n tay thoải m&aacute;i. Ho&agrave;n hảo để t&ocirc; điểm cho phong c&aacute;ch trang tr&iacute; nh&agrave; bạn v&agrave; phục vụ đồ uống lạnh với phong c&aacute;ch. Dễ d&agrave;ng phối với mọi kiểu trang tr&iacute; b&agrave;n ăn trong nh&agrave; hoặc quầy bar c&agrave; ph&ecirc;.</p>

<p>3. Thiết kế miệng rộng đặc biệt gi&uacute;p cốc thủy tinh dễ l&agrave;m sạch. Th&agrave;nh trong nhẵn sẽ kh&ocirc;ng hấp thụ thức ăn hoặc chất m&agrave;u. C&oacute; thể dễ d&agrave;ng l&agrave;m sạch cốc uống nước trong suốt n&agrave;y bằng c&aacute;ch rửa tay v&agrave; c&oacute; thể rửa bằng m&aacute;y rửa ch&eacute;n để thuận tiện cho bạn.</p>

<p>4. Cốc uống nước thủy tinh th&iacute;ch hợp cho mọi loại đồ uống n&oacute;ng v&agrave; lạnh. Cốc n&agrave;y ph&ugrave; hợp để phục vụ v&agrave; thưởng thức, rượu whisky, rượu vang, cocktail hoặc thậm ch&iacute; l&agrave; sữa, nước tr&aacute;i c&acirc;y, c&agrave; ph&ecirc; .</p>

<p>5.Cốc nước l&agrave; m&oacute;n qu&agrave; ho&agrave;n hảo cho t&acirc;n gia, tiệc t&ugrave;ng, h&agrave;ng x&oacute;m, ng&agrave;y kỷ niệm, v.v. Đ&acirc;y sẽ l&agrave; một m&oacute;n qu&agrave; tuyệt vời th&iacute;ch hợp cho người y&ecirc;u c&agrave; ph&ecirc; v&agrave; tr&agrave; để l&agrave;m qu&agrave; tặng sinh nhật, Ng&agrave;y của mẹ, Ng&agrave;y của cha, Lễ tạ ơn, Năm mới Gi&aacute;ng sinh.</p>

<h2>Ưu v&agrave; nhược điểm của kệ đựng ch&eacute;n đĩa cố định v&agrave; di động</h2>

<h3><strong><strong>kệ đựng ch&eacute;n đĩa&nbsp;</strong></strong><strong><strong>cố định</strong></strong></h3>

<h3>Ưu điểm:</h3>

<ul>
	<li>Gi&aacute; th&agrave;nh rẻ.</li>
	<li>Chắc chắn, ki&ecirc;n cố, khả năng chịu tải trọng cao tốt do được gắn trực tiếp v&agrave;o th&agrave;nh tủ.</li>
</ul>

<h4>Nhược điểm:</h4>

<ul>
	<li>Kh&ocirc;ng n&acirc;ng l&ecirc;n hạ xuống được n&ecirc;n kh&oacute; khăn hơn mỗi lần &uacute;p v&agrave; lấy b&aacute;t.</li>
	<li>K&eacute;n chọn chiều cao: Chỉ ph&ugrave; hợp với những người c&oacute; chiều cao tương th&iacute;ch, kh&ocirc;ng ph&ugrave; hợp với mọi đối tượng nhất l&agrave; trẻ em v&agrave; người gi&agrave;.</li>
</ul>

<h3><strong><strong>kệ đựng ch&eacute;n đĩa di động &ndash; kệ để ch&eacute;n đĩa th&ocirc;ng minh</strong></strong></h3>

<h4>Ưu điểm:</h4>

<ul>
	<li>Kh&ocirc;ng k&eacute;n chiều cao: thiết kế th&ocirc;ng minh cho ph&eacute;p n&acirc;ng l&ecirc;n hạ xuống dễ d&agrave;ng n&ecirc;n mọi đối tượng đều c&oacute; thể sử dụng. Ngo&agrave;i ra, gi&aacute; b&aacute;t n&agrave;y gi&uacute;p <strong>r&aacute;o nước</strong> nhanh hơn.</li>
	<li>Mẫu m&atilde; sang trọng, hiện đại.</li>
</ul>

<h4>Nhược điểm:</h4>

<ul>
	<li>Gi&aacute; cao hơn nhiều so với gi&aacute; b&aacute;t cố định.</li>
	<li>V&igrave; gi&aacute; th&agrave;nh cao v&agrave; nhu cầu ng&agrave;y c&agrave;ng nhiều n&ecirc;n hiện nay c&oacute; rất nhiều sản phẩm giả, fake, h&agrave;ng kh&ocirc;ng r&otilde; nguồn gốc, k&eacute;m chất lượng, h&agrave;ng dễ han gỉ v&agrave; nhanh hỏng&hellip;Điều n&agrave;y khiến kh&aacute;ch h&agrave;ng kh&oacute; khăn khi t&igrave;m mua những sản phẩm ch&iacute;nh h&atilde;ng, chất lượng.</li>
</ul>

<p>Vậy &ldquo;t&ocirc;i n&ecirc;n chọn gi&aacute; cố định hay di động?&rdquo; l&agrave; c&acirc;u hỏi được nhiều người quan t&acirc;m nhất hiện nay, nếu bạn cũng đang thắc mắc như tr&ecirc;n, mời bạn k&eacute;o tiếp xuống dưới để c&ugrave;ng ch&uacute;ng t&ocirc;i t&igrave;m c&acirc;u trả lời nh&eacute;!</p>

<h3><strong><strong>kệ đựng ch&eacute;n </strong></strong><strong>đĩa&nbsp;tr&ecirc;n chậu rửa</strong></h3>

<p>Kh&ocirc;ng chi c&oacute; 2 d&ograve;ng sản phẩm gi&aacute; để b&aacute;t tr&ecirc;n tủ m&agrave;&nbsp; ch&uacute;ng t&ocirc;i ph&aacute;t triển th&ecirc;m 1 d&ograve;ng sản phẩm gi&aacute; để b&aacute;t đĩa tr&ecirc;n chậu rửa. D&ograve;ng sản phẩm n&agrave;y được c&aacute;c hộ gia đ&igrave;nh nhỏ rất ưa chuộng bởi đặc t&iacute;nh dễ di chuyển của ch&uacute;ng.</p>

<h2><strong>kệ đựng ch&eacute;n</strong><strong> đĩa inox nan &acirc;m tủ gắn c&aacute;nh ray giảm chấn</strong></h2>

<p>L&agrave; loại gi&aacute; được thiết kế 3 kiểu nan với 3 chức năng &uacute;p b&aacute;t ăn cơm, &uacute;p đĩa v&agrave; phần nan c&ograve;n lại bằng phẳng c&oacute; thể để đĩa, t&ocirc;, b&aacute;t, xoong nồi t&ugrave;y &yacute; gia chủ. L&ograve;ng gi&aacute; rộng r&atilde;i, độ s&acirc;u vừa phải c&oacute; thể để được &iacute;t nhất 10 chiếc đĩa, 12 chiếc b&aacute;t ăn cơm v&agrave; th&ecirc;m một lượng kha kh&aacute; tầm 5-10 chiếc b&aacute;t t&ocirc; to hoặc c&aacute;c loại vật dụng kh&aacute;c một c&aacute;ch tiện lợi. Ngo&agrave;i ra gi&aacute; c&ograve;n được t&iacute;ch hợp với ray giảm chấn vừa tiện sử dụng lại c&oacute; khả năng triệt ti&ecirc;u được mọi va chạm n&ecirc;n an to&agrave;n khi để nhiều b&aacute;t đĩa c&ugrave;ng l&uacute;c.</p>

<p>Đ&acirc;y l&agrave; chiếc gi&aacute; ph&ugrave; hợp với những gia đ&igrave;nh trẻ &iacute;t người &iacute;t b&aacute;t đĩa v&iacute; dụ như 2 vợ chồng v&agrave; một đến 2 em b&eacute;. Ngo&agrave;i ra, nếu bạn cần một chiếc gi&aacute; đa năng để được c&ugrave;ng l&uacute;c cả b&aacute;t ăn cơm, b&aacute;t t&ocirc;, đĩa thường d&ugrave;ng hằng ng&agrave;y, bạn cũng c&oacute; thể chọn sản phẩm n&agrave;y.</p>

<h3>5 căn cứ chọn&nbsp;kệ đựng ch&eacute;n đĩa cố định &nbsp;hay kệ đựng ch&eacute;n đĩa di động?</h3>

<p>Để trả lời được c&acirc;u hỏi tr&ecirc;n, bạn cần căn cứ v&agrave;o điều kiện cụ thể của gia đ&igrave;nh m&igrave;nh. Dưới đ&acirc;y l&agrave; 5 gợi &yacute; gi&uacute;p bạn tự t&igrave;m c&acirc;u trả lời cho ch&iacute;nh m&igrave;nh:</p>

<ol>
	<li><em><strong><strong>Điều kiện t&agrave;i ch&iacute;nh</strong></strong></em>: Nếu điều kiện t&agrave;i ch&iacute;nh nh&agrave; bạn đang dư giả, bạn n&ecirc;n c&acirc;n nhắc đến việc đầu tư&nbsp;gi&aacute; b&aacute;t di động th&ocirc;ng minh, tiện lợi gi&uacute;p c&ocirc;ng việc bếp n&uacute;c trở n&ecirc;n đơn giản, dễ d&agrave;ng hơn.</li>
	<li><em><strong><strong>Sở th&iacute;ch c&aacute; nh&acirc;n</strong></strong></em>: Tất nhi&ecirc;n l&agrave; hầu hết c&aacute;c chị em ai cũng muốn sở hữu những chiếc gi&aacute; b&aacute;t di động đẹp mắt, tiện nghi, sang trọng hơn l&agrave; một chiếc gi&aacute; cố định th&ocirc;ng thường.</li>
	<li><em><strong><strong>Người đầu bếp ch&iacute;nh của gia đ&igrave;nh</strong></strong></em>: Nếu bạn ở c&ugrave;ng bố mẹ hoặc bố mẹ chồng, bạn thường xuy&ecirc;n bận rộn v&agrave; mẹ bạn l&agrave; người nội trợ ch&iacute;nh, bạn n&ecirc;n chọn gi&aacute; b&aacute;t di động để gi&uacute;p mẹ bạn dễ d&agrave;ng lấy/&uacute;p b&aacute;t đĩa khi cần sử dụng.</li>
	<li><strong><strong><em>Phong c&aacute;ch nội thất của căn bếp</em></strong></strong>: Với một căn bếp hiện đại, được <strong>thiết kế đẹp</strong>, tiện nghi, gi&aacute; b&aacute;t di động l&agrave; lựa chọn hợp l&yacute;. Ngược lại,kệ đựng ch&eacute;n cố định&nbsp;sẽ l&agrave; lựa chọn th&ocirc;ng minh v&agrave; tiết kiệm nếu bạn sở hữu một căn bếp b&igrave;nh thường, kh&ocirc;ng đầu tư nội thất xịn, đẹp ngay từ đầu.</li>
	<li><strong><strong><em>Nh&agrave; cung cấp uy t&iacute;n, ch&iacute;nh h&atilde;ng</em></strong></strong>: D&ugrave; l&agrave; chọn gi&aacute; b&aacute;t cố định hay di động, m&agrave; đặc biệt l&agrave; gi&aacute; b&aacute;t di động, bạn chỉ n&ecirc;n mua khi thực sự t&igrave;m được c&aacute;c sản phẩm chất lượng, ch&iacute;nh h&atilde;ng, nh&agrave; cung cấp uy t&iacute;n l&acirc;u năm tr&ecirc;n thị trường. Tr&aacute;nh mua h&agrave;ng tr&ocirc;i nổi, h&agrave;ng kh&ocirc;ng r&otilde; nguồn gốc, h&agrave;ng giả, h&agrave;ng nh&aacute;i k&eacute;m chất lượng.</li>
</ol>

<p>Gia Dụng S&agrave;i G&ograve;n lu&ocirc;n nhiệt t&igrave;nh đ&oacute;n nhận những &yacute; kiến đ&oacute;ng g&oacute;p từ ph&iacute;a Kh&aacute;ch h&agrave;ng th&ocirc;ng qua số điện thoại:&nbsp;<strong>0989012444 &ndash; 0979686680&nbsp;</strong>hoặc fanpage:&nbsp;<a href="https://www.facebook.com/GiaDungSaiGonTNL" rel="noopener" target="_blank">Gia Dụng S&agrave;i G&ograve;n</a>&nbsp;để ng&agrave;y c&agrave;ng cải tiến v&agrave; n&acirc;ng cao hơn nữa chất lượng phục vụ.</p>

<p>Như vậy, t&ugrave;y điều kiện cụ thể của mỗi gia đ&igrave;nh, c&aacute; nh&acirc;n m&agrave; chọn lựa <strong>mẫu gi&aacute; để b&aacute;t đĩa xoong nồi </strong>kh&aacute;c nhau. Tuy nhi&ecirc;n, c&oacute; một lời khuy&ecirc;n cho bạn l&agrave; cho d&ugrave; bạn chọn loại n&agrave;o th&igrave; cũng n&ecirc;n t&igrave;m mua đ&uacute;ng sản phẩm ch&iacute;nh h&atilde;ng từ c&aacute;c nh&agrave; cung cấp uy t&iacute;n l&acirc;u năm tr&ecirc;n thị trường, tr&aacute;nh mua phải h&agrave;ng giả, h&agrave;ng nh&aacute;i k&eacute;m chất lượng, nhanh han gỉ, dễ hỏng h&oacute;c.</p>

<p>&nbsp;</p>
</div>
</div>
', N'Kệ đựng chén đĩa là phụ kiện không thể thiếu trong mỗi không gian nhà bếp của các gia đình. Đây là dòng sản phẩm hỗ trợ công việc bếp núc rất tuyệt vời, giúp..... Khuyến mãi khi mua 1 kệ đựng chén đũa bất kì, tặng ngay một cốc thủy tinh uống nước nhật bản					', CAST(N'2024-04-05T15:56:16.4756468' AS DateTime2), CAST(N'2024-05-28T01:33:06.9789307' AS DateTime2), 0, 1)
INSERT [dbo].[posts] ([id], [title], [slug], [avatar], [content], [short_content], [created_at], [updated_at], [status], [author]) VALUES (5, N'Xẻng trồng cây đa năng thích hợp với làm vườn và chăm sóc cây trồng', N'x?ng-tr?ng-cây-da-nang-thích-h?p-v?i-làm-vu?n-và-cham-sóc-cây-tr?ng', N'xeng.png', N'<p><em><strong>Ưu điểm khi sử dụng</strong></em><br />
-&nbsp;Bộ dụng cụ l&agrave;m vườn gi&uacute;p cho c&ocirc;ng việc l&agrave;m vườn trở n&ecirc;n đơn giản hơn.<br />
- Thiết kế nhỏ gọn, vừa với l&ograve;ng b&agrave;n tay v&agrave; dễ d&agrave;ng cất giữ.<br />
- Dễ d&agrave;ng sử dụng, với xẻng l&agrave;m vườn, vườn hoa,&nbsp;vườn rau của bạn sẽ lu&ocirc;n tươi xanh.<br />
- Được l&agrave;m từ chất liệu th&eacute;p cao cấp bền chắc.<br />
- Với&nbsp;bộ dụng cụ l&agrave;m vườn mini gi&uacute;p cho bạn th&ecirc;m y&ecirc;u khu vườn của m&igrave;nh hơn.</p>

<p>-&nbsp;<em><strong>Bộ Dụng Cụ, Đồ L&agrave;m Vườn Chăm S&oacute;c C&acirc;y Cảnh Mini&nbsp;</strong></em>c&oacute; k&iacute;ch thước lớn hơn so với c&aacute;c loại l&agrave;m vườn th&ocirc;ng thường&nbsp;</p>

<p>Trong qu&aacute; tr&igrave;nh l&agrave;m vườn tại nh&agrave; , bạn sẽ cho trẻ nhỏ tham gia qu&aacute; tr&igrave;nh trồng rau hay trồng hoa để trẻ sẽ gần gũi với thi&ecirc;n nhi&ecirc;n hơn, hiểu biết th&ecirc;m về thế giới xung quanh trẻ, gi&aacute;o dục trẻ y&ecirc;u lao động. Trẻ cần d&ugrave;ng những dụng cụ l&agrave;m vườn cho b&eacute; th&igrave; bộ dụng cụ n&agrave;y cũng rất ph&ugrave; hợp.</p>

<h3>Đặc điểm nổi bật</h3>

<ul>
	<li>Dụng cụ được thiết kế xinh xắn, nhỏ gọn</li>
	<li>Bộ sản phẩm được l&agrave;m từ kim loại cứng c&aacute;p, bền khỏe</li>
	<li>C&aacute;n cầm chống trơn trượt</li>
	<li>Gi&uacute;p đất tơi xốp một c&aacute;ch dễ d&agrave;ng,tạo m&ocirc;i trường th&ocirc;ng tho&aacute;ng cho c&acirc;y ph&aacute;t triển</li>
	<li>Tha hồ đ&agrave;o , xới, trộn , x&uacute;c với dụng cụ trọng bộ sản phẩm</li>
	<li>Sơn tĩnh điện bề mặt, bảo vệ sản phẩm.</li>
</ul>
', N'gày nay, phong trào làm vườn trên sân thượng dang phát triển. Các dụng cụ làm vườn mini vừa nhỏ gọn, dễ cất mà vẫn đáp ứng được việc canh tác trong vườn phố. Trong hành trình làm vườn trên sân thượng chúng ta luôn phải xới đất, đào đất, trộn đất. Khi đó Bộ Dụng Cụ, Đồ Làm Vườn Chăm Sóc Cây Cảnh Mini sẽ giúp chúng ta trong những công việc này.', CAST(N'2024-05-17T14:27:49.3759718' AS DateTime2), CAST(N'2024-05-27T13:52:40.2401117' AS DateTime2), 0, 1)
SET IDENTITY_INSERT [dbo].[posts] OFF
GO
SET IDENTITY_INSERT [dbo].[products] ON 

INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (3, N'Giá treo đồ nhà bếp đa năng 50cm', 1, CAST(110000 AS Decimal(18, 0)), N'<div class="entry-content panel">
<p>Gi&aacute; treo đồ nh&agrave; bếp ch&iacute;nh l&agrave; giải ph&aacute;p gi&uacute;p bạn sắp xếp ngăn nắp đồ d&ugrave;ng nh&agrave; bếp. Đồ d&ugrave;ng nh&agrave; bếp kh&ocirc;ng c&ograve;n bị vứt lung tung khiến bạn mất thời gian trong việc t&igrave;m kiếm, kh&ocirc;ng lộn xộn. Sản phẩm được thiết kế hiện đại, gọn nhẹ với chất liệu inox kh&ocirc;ng gỉ chắc chắn cực bền gi&uacute;p kh&ocirc;ng gian bếp nh&agrave; bạn th&ecirc;m tho&aacute;ng đ&atilde;ng, vệ sinh.</p>
</div>
', 1, CAST(150000 AS Decimal(18, 0)), NULL, N'', CAST(90000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 20, NULL, N'<p>Gi&aacute; treo đồ nh&agrave; bếp ch&iacute;nh l&agrave; giải ph&aacute;p gi&uacute;p bạn sắp xếp ngăn nắp đồ d&ugrave;ng nh&agrave; bếp. Đồ d&ugrave;ng nh&agrave; bếp kh&ocirc;ng c&ograve;n bị vứt lung tung khiến bạn mất thời gian trong việc t&igrave;m kiếm, kh&ocirc;ng lộn xộn. Sản phẩm được thiết kế hiện đại, gọn nhẹ với chất liệu inox kh&ocirc;ng gỉ chắc chắn cực bền gi&uacute;p kh&ocirc;ng gian bếp nh&agrave; bạn th&ecirc;m tho&aacute;ng đ&atilde;ng, vệ sinh.</p>
', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (4, N'Giỏ đựng hoa quả 2 tầng có thể tháo rời', 1, CAST(270000 AS Decimal(18, 0)), N'<div class="panel entry-content">
				

<p><span class="text">Giỏ 2 tầng có thể tháo rời rất gọn và tiện lợi nha các mẹ. </span></p>
<p><span class="text">Để hoa quả tiếp khách thì cứ phải gọi là điểm nhấn luôn. </span></p>
<p><span class="text">&nbsp;Có thể làm khay để cốc, chén cũng được ạ. </span></p>
<ul>
<li><span class="text">Màu : Đen – Trắng </span></li>
<li><span class="text">Kích thước xem ảnh giúp em nha </span></li>
<li><span class="text">Chất liệu : Sắt sơn tĩnh điện,tay cầm gỗ.</span></li>
</ul>
			</div>', 2, CAST(310000 AS Decimal(18, 0)), NULL, NULL, CAST(170000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 50, NULL, N'<p>Chất liệu : Sắt sơn tĩnh điện, tay cầm gỗ.</p>', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (5, N'Kệ để chén bát đĩa đa năng trên bồn rửa', 1, CAST(790000 AS Decimal(18, 0)), N'<div class="panel entry-content">
				

<p>Kệ để chén / bát / đĩa đa năng trên bồn rửa chén giúp tiết kiệm không gian, tiện lợi trong quá trình rửa chén và ráo nước cho các đồ dùng phòng bếp nhanh chóng. Được làm từ thép carbon, sơn tĩnh điện giúp chống gỉ và trầy xước. Đầy đủ phụ kiện như đựng đũa, dao, thớt, đồ rửa chén, móc treo… với sự lựa chọn 1 tầng và 2 tầng phù hợp cho nhiều nhu cầu tuỳ không gian</p>
			</div>', 1, CAST(950000 AS Decimal(18, 0)), NULL, N'', CAST(190000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 60, NULL, N'<p>Kệ để chén / bát / đĩa đa năng trên bồn rửa chén giúp tiết kiệm không gian, tiện lợi trong quá trình rửa chén và ráo nước cho các đồ dùng phòng bếp nhanh chóng. Được làm từ thép carbon, sơn tĩnh điện giúp chống gỉ và trầy xước. Đầy đủ phụ kiện như đựng đũa, dao, thớt, đồ rửa chén, móc treo… với sự lựa chọn 1 tầng và 2 tầng phù hợp cho nhiều nhu cầu tuỳ không gian</p>', 1)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (6, N'Kệ sấy và khử khuẩn đũa thìa sấy khô nhanh và diệt mọi vi khuẩn', 1, CAST(245000 AS Decimal(18, 0)), N'<div class="entry-content panel">
<p>Kệ sấy v&agrave; khử khuẩn đũa th&igrave;a sấy kh&ocirc; nhanh v&agrave; diệt mọi vi khuẩn tr&ecirc;n dụng cụ đũa th&igrave;a với chất liệu nhựa cao cấp an to&agrave;n.</p>

<p>Chất liệu: Nhựa PP +Pc cao cấp K&iacute;ch Thước : 21 x8 x 30cm</p>

<p>Sấy kh&ocirc; nhanh v&agrave; diệt mọi vi khuẩn tr&ecirc;n đũa th&igrave;a Hộp sấy v&agrave; khử khuẩn đũa th&igrave;a, Hộp đựng đũa th&igrave;a, gi&aacute; cắm đũa thia nh&agrave; bếp, ống cắm dao đũa th&igrave;a, ống đựng đũa, kệ nh&agrave; bếp, gi&aacute; để đồ nh&agrave; bếp Dụng cụ nh&agrave; bếp, kệ đựng dao đũa muỗng , gi&aacute; cắm dao, gi&aacute; g&agrave;i dao</p>
</div>
', 2, CAST(320000 AS Decimal(18, 0)), NULL, N'', CAST(145000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 10, NULL, N'<p>Sấy kh&ocirc; nhanh v&agrave; diệt mọi vi khuẩn tr&ecirc;n đũa th&igrave;a Hộp sấy v&agrave; khử khuẩn đũa th&igrave;a, Hộp đựng đũa th&igrave;a, gi&aacute; cắm đũa thia nh&agrave; bếp, ống cắm dao đũa th&igrave;a, ống đựng đũa, kệ nh&agrave; bếp, gi&aacute; để đồ nh&agrave; bếp Dụng cụ nh&agrave; bếp, kệ đựng dao đũa muỗng , gi&aacute; cắm dao, gi&aacute; g&agrave;i dao</p>
', 2)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (7, N'Máy sấy tiệt trùng dao thớt đũa Bear', 1, CAST(890000 AS Decimal(18, 0)), N'<div class="entry-content panel">
<p>hời tiết ẩm nồm của Việt Nam lu&ocirc;n l&agrave; m&ocirc;i trường dễ sinh s&ocirc;i nấm mốc, c&aacute;c vật dụng h&agrave;ng ng&agrave;y như dao, đũa, thớt,&hellip; nếu kh&ocirc;ng kh&ocirc; cong v&agrave; sạch vi khuẩn sẽ rất dễ g&acirc;y nguy hại cho sức khoẻ. M&aacute;y sấy tiệt tr&ugrave;ng dao, thớt, đũa Bear l&agrave; sản phẩm h&agrave;ng đầu gi&uacute;p sấy kh&ocirc;, loại bỏ tới 99,99% vi khuẩn g&acirc;y hại. M&aacute;y sấy tiệt tr&ugrave;ng dao thớt đũa Bear:</p>

<p>&nbsp;3in1: sấy kh&ocirc;, khử khuẩn tia UV, gi&aacute; đựng bảo quản</p>

<p>&nbsp;Chứng nhận tiệt tr&ugrave;ng tia UV tới 99,99%</p>

<p>&nbsp;Sấy kh&ocirc; kh&iacute; n&oacute;ng 70&deg;C, chống ẩm mốc, cong v&ecirc;nh</p>

<p>Chu tr&igrave;nh sấy tiết kiệm: thủ c&ocirc;ng &amp; tự động</p>

<p>&nbsp;C&ocirc;ng nghệ điều khiển cảm ứng 1 chạm</p>

<p>&nbsp;Khay để dao, thớt, đũa dễ d&agrave;ng th&aacute;o rời vệ sinh</p>
</div>
', 1, CAST(1290000 AS Decimal(18, 0)), NULL, N'', CAST(190000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 80, NULL, N'<ul>
	<li>&nbsp;3in1: sấy kh&ocirc;, khử khuẩn tia UV, gi&aacute; đựng bảo quản</li>
	<li>&nbsp;Chứng nhận tiệt tr&ugrave;ng tia UV tới 99,99%</li>
	<li>&nbsp;Sấy kh&ocirc; kh&iacute; n&oacute;ng 70&deg;C, chống ẩm mốc, cong v&ecirc;nh</li>
	<li>Chu tr&igrave;nh sấy tiết kiệm: thủ c&ocirc;ng &amp; tự động</li>
	<li>&nbsp;C&ocirc;ng nghệ điều khiển cảm ứng 1 chạm</li>
	<li>&nbsp;Khay để dao, thớt, đũa dễ d&agrave;ng th&aacute;o rời vệ sinh</li>
</ul>
', 1)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (8, N'Thùng Rác Thông Minh TOWNEW T AIR X ( BLUE) – Bản Quốc Tế', 2, CAST(2880000 AS Decimal(18, 0)), N'<div class="entry-content panel">
<p>Th&ugrave;ng r&aacute;c th&ocirc;ng minh Townew T Air X l&agrave; sản phẩm đến từ thương hiệu điện tử nổi tiếng tại Trung Quốc.</p>

<p>Với nhiều t&iacute;nh năng ưu việt được nghi&ecirc;n cứu v&agrave; ph&aacute;t triển dựa tr&ecirc;n th&oacute;i quen sử dụng của con người, th&ugrave;ng r&aacute;c th&ocirc;ng minh Townew gi&uacute;p tiết kiệm thời gian dọn dẹp v&agrave; giữ kh&ocirc;ng gian nh&agrave; ở lu&ocirc;n sạch sẽ tho&aacute;ng m&aacute;t.</p>

<p>Với thiết kế nhỏ gọn tiện dụng t&iacute;ch hợp ứng dụng c&ocirc;ng nghệ th&ocirc;ng minh, th&ugrave;ng r&aacute;c Townew T Air X ng&agrave;y c&agrave;ng được sử dụng phổ biến cho hệ thống nh&agrave; ở th&ocirc;ng minh (Smart home).</p>

<p>Sự ph&aacute;t triển mạnh mẽ của kỷ nguy&ecirc;n c&ocirc;ng nghệ 4.0 cho ph&eacute;p c&aacute;c thương hiệu điện tử ứng dụng c&ocirc;ng nghệ th&ocirc;ng minh trong c&aacute;c sản phẩm nội thất nh&agrave; ở. Nhờ đ&oacute; gi&uacute;p cho cuộc sống con người ng&agrave;y c&agrave;ng tiện dụng v&agrave; hiện đại.</p>

<p>Th&ugrave;ng r&aacute;c th&ocirc;ng minh Towne T Air X l&agrave; một trong những sản phẩm nội thất th&ocirc;ng minh với c&aacute;c ưu điểm nổi bật.</p>

<p>Thời lượng sử dụng pin chắc hẳn l&agrave; điều m&agrave; tất cả những ai sử dụng sản phẩm điện tử đều quan t&acirc;m v&agrave; t&igrave;m hiểu kỹ lưỡng trước khi đưa ra quyết định mua. Hiểu được điều đ&oacute;, nh&agrave; sản xuất th&ugrave;ng r&aacute;c th&ocirc;ng minh Townew đ&atilde; c&oacute; những cải tiến c&ocirc;ng nghệ đột ph&aacute; sau đ&acirc;y:</p>

<p>Thiết kế dung lượng pin lithium c&oacute; thể sử dụng trong 30 ng&agrave;y chỉ với 10 giờ sạc thực sự l&agrave; một ưu điểm lớn gi&uacute;p người d&ugrave;ng tiết kiệm thời gian v&agrave; c&ocirc;ng sức.</p>

<p>Thiết kế cổng USB kh&aacute; phổ biến gi&uacute;p cho người d&ugrave;ng c&oacute; thể dễ d&agrave;ng thay thế c&aacute;p sạc khi cần thiết.</p>

<p>Nguồn pin t&iacute;ch hợp, thời lượng pin ổn định v&agrave; l&acirc;u d&agrave;i gi&uacute;p cho người sử dụng chỉ cần cắm sạc tối thiểu 1 lần trong mỗi th&aacute;ng.</p>

<p>Nhờ v&agrave;o c&ocirc;ng nghệ hiện đại v&agrave; thiết kế tinh giản gọn nhẹ, cơ chế cảm biến chống tiếng ồn l&ecirc;n đến 100%. Mang đến kh&ocirc;ng gian nh&agrave; ở tĩnh lặng v&agrave; b&igrave;nh y&ecirc;n.</p>

<p>B&ecirc;n cạnh đ&oacute;, t&iacute;nh năng tự động đ&oacute;ng g&oacute;i t&uacute;i r&aacute;c gi&uacute;p chủ nh&acirc;n ng&ocirc;i nh&agrave; tiết kiệm thời gian dọn dẹp v&agrave; dễ d&agrave;ng chăm s&oacute;c kh&ocirc;ng gian sống lu&ocirc;n gọn g&agrave;ng sạch sẽ.</p>

<p>Hệ thống đ&oacute;ng mở bằng cảm biến tự động hẳn l&agrave; một ưu thế nổi trội của sản phẩm th&ugrave;ng r&aacute;c th&ocirc;ng minh n&agrave;y:</p>

<p>Phạm vi cảm biến 35cm: Phạm vi n&agrave;y cho ph&eacute;p người d&ugrave;ng c&oacute; thể dễ d&agrave;ng k&iacute;ch hoạt cảm biến mở đ&oacute;ng nhanh ch&oacute;ng v&agrave; tiện dụng.</p>

<p>Tốc độ phản hồi chỉ với 0,3 gi&acirc;y: Tốc độ phản hồi của cảm biến ngay khi c&oacute; chuyển động trong phạm vị 35cm.</p>
</div>
', 1, CAST(3080000 AS Decimal(18, 0)), NULL, NULL, CAST(1080000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 100, NULL, N'<p>Với nhiều t&iacute;nh năng ưu việt được nghi&ecirc;n cứu v&agrave; ph&aacute;t triển dựa tr&ecirc;n th&oacute;i quen sử dụng của con người, th&ugrave;ng r&aacute;c th&ocirc;ng minh Townew gi&uacute;p tiết kiệm thời gian dọn dẹp v&agrave; giữ kh&ocirc;ng gian nh&agrave; ở lu&ocirc;n sạch sẽ tho&aacute;ng m&aacute;t.</p>

<p>Với thiết kế nhỏ gọn tiện dụng t&iacute;ch hợp ứng dụng c&ocirc;ng nghệ th&ocirc;ng minh, th&ugrave;ng r&aacute;c Townew T Air X ng&agrave;y c&agrave;ng được sử dụng phổ biến cho hệ thống nh&agrave; ở th&ocirc;ng minh (Smart home).</p>
', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (9, N'Hộp đựng giấy vệ sinh dán tường Ecoco', 3, CAST(170000 AS Decimal(18, 0)), N'<div class="panel entry-content">
				

<p>Hộp đựng giấy vệ sinh dán tường Ecoco hai ngăn, chống thấm nước là hàng nhập khẩu chính hãng cao cấp, được Gia Dụng Thông Minh cam kết về chất lượng và giá cả tốt do nhập khẩu số lượng lớn. Thiết kế 2 ngăn, có thể dùng cho mọi loại giấy vệ sinh như loại vuông, loại chữ nhật hay cuộn tròn đều phù hợp. Dán tường chắc chắn, không cần khoan tường.</p>
			</div>', 1, CAST(270000 AS Decimal(18, 0)), NULL, NULL, CAST(70000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 60, NULL, N'<p>Thiết kế 2 ngăn, có thể dùng cho mọi loại giấy vệ sinh như loại vuông, loại chữ nhật hay cuộn tròn đều phù hợp. Dán tường chắc chắn, không cần khoan tường.</p>', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (10, N'Hộp đựng giấy vệ sinh Oenon mẫu mới', 3, CAST(170000 AS Decimal(18, 0)), N'<div class="panel entry-content">
				

<p>Hộp đựng giấy vệ sinh Oenon mẫu mới. Ấn để mở nắp hộp một cách dễ dàng. Hộp được thiết kế 2 ngăn tiện lợi.</p>
<p>Có nhiều ngăn để chứa đồ cá nhân. Ngăn để giấy rút dễ dàng tháo lắp. Khe rút giấy có bánh răng rất thuận tiện để lấy giấy.</p>
<p>Hộp có khay để điện thoại cũng như các vật dụng cá nhân rất chắc chắn và tiện dụng. Không cần khoan tường bắt vít mà dùng miếng dán cao cấp đi kèm sản phẩm. Chịu tải trọng cao. sản phẩm có thể sử dụng trong nhà hàng, nhà tắm, nhà vệ sinh.</p>
<p>&nbsp;</p>
			</div>', 1, CAST(210000 AS Decimal(18, 0)), NULL, NULL, CAST(70000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 80, NULL, N'<p>Hộp có khay để điện thoại cũng như các vật dụng cá nhân rất chắc chắn và tiện dụng. Không cần khoan tường bắt vít mà dùng miếng dán cao cấp đi kèm sản phẩm. Chịu tải trọng cao. sản phẩm có thể sử dụng trong nhà hàng, nhà tắm, nhà vệ sinh.</p>', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (11, N'Kệ nhả kem phòng tắm thông minh kèm cốc hút từ tính', 3, CAST(230000 AS Decimal(18, 0)), N'<div class="entry-content panel">
<p>B&agrave;n chải đ&aacute;nh răng, kem đ&aacute;nh răng sử dụng h&agrave;ng ng&agrave;y v&agrave; đồ d&ugrave;ng c&aacute; nh&acirc;n tr&ecirc;n b&agrave;n m&aacute;y t&iacute;nh hoặc b&agrave;n trang điểm của bạn thường hay để lung tung kh&ocirc;ng c&oacute; khay đựng.</p>

<p>B&acirc;y giờ bạn c&oacute; thể thay đổi điều đ&oacute; để cho căn ph&ograve;ng v&agrave; b&agrave;n l&agrave;m việc của bạn gọn g&agrave;n v&agrave; tiện dụng hơn bằng sản phẩm của ch&uacute;ng t&ocirc;i cung cấp.</p>

<h3>Hộp đựng b&agrave;n chải, nhả kem đ&aacute;nh răng treo tường</h3>

<p>Treo được 4 cốc. Treo 4 b&agrave;n chải đ&aacute;nh răng. C&oacute; r&atilde;nh đựng dao cạo r&acirc;u, lược chải t&oacute;c. Hộp dễ d&agrave;ng th&aacute;o lắp để vệ sinh. Miếng d&aacute;n c&ocirc;ng nghệ mới, si&ecirc;u chắc v&agrave; bền theo thời gian(Kh&ocirc;ng lo tự dơi như c&aacute;c miếp h&uacute;t ch&acirc;n kh&ocirc;ng th&ocirc;ng thường). Chất liệu nhựa ABS, PP si&ecirc;u bền v&agrave; đẹp</p>

<ul>
	<li><strong>K&iacute;ch thước đ&oacute;ng g&oacute;i:</strong> 384mm x 113mm x 200mm</li>
	<li><strong>Trọng lượng:</strong> 1020g</li>
</ul>

<p>Đ&acirc;y l&agrave; h&agrave;ng cao cấp của thương hiệu ECOCO, d&agrave;nh cho thị trường Nhật Bản</p>

<p>C&aacute;c chi tiết cực kỳ tinh xảo, cẩn thận. Chất nhựa đẹp, l&agrave; nhựa nano kh&aacute;ng khuẩn, chống b&aacute;m bẩn.*</p>

<h3>Hướng dẫn lắp đặt</h3>

<ul>
	<li>Bước 1: L&agrave;m sạch bề mặt tường (vị tr&iacute; lắp đặt kệ)</li>
	<li>Bước 2: Lắp miếng d&aacute;n v&agrave;o kệ &amp; loại bỏ m&agrave;ng bảo vệ miếng d&aacute;n</li>
	<li>Bước 3: D&aacute;n kệ l&ecirc;n tường (vị tr&iacute; lắp đặt)</li>
	<li>Bước 4: Nhấc kệ ra &amp; d&ugrave;ng tay &eacute;p hết kh&ocirc;ng kh&iacute; trong miếng d&aacute;n</li>
	<li>Bước 5: Cố định miếng d&aacute;n trong 12 giờ đồng hồ</li>
	<li>Bước 6: Gắn kệ v&agrave;o vị tr&iacute; cố định v&agrave; cho c&aacute;c vật dụng v&agrave;o (Đảm bảo 12 giờ sau khi miếng d&aacute;n d&iacute;nh v&agrave;o tường).</li>
</ul>

<h3>Cam kết với kh&aacute;ch h&agrave;ng</h3>

<p>Sản phẩm đ&uacute;ng th&ocirc;ng tin m&ocirc; tả</p>
</div>
', 1, CAST(330000 AS Decimal(18, 0)), NULL, NULL, CAST(130000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 60, NULL, N'<ul>
	<li>Trang tr&iacute; cho kh&ocirc;ng gian ph&ograve;ng tắm th&ecirc;m tiện nghi.</li>
	<li>Dễ d&agrave;ng lắp đặt, kh&ocirc;ng cần khoan đục.</li>
	<li>Bền đẹp, chắc chắn, chịu lực tốt TỐI ĐA 6KG.</li>
	<li>Cốc từ h&uacute;t ngược hiện đại gi&uacute;p vật dụng lu&ocirc;n sạch sẽ v&agrave; kh&ocirc; r&aacute;o.</li>
	<li>B&agrave;n chải được gắn trong kh&ocirc;ng gian k&iacute;n chắn bụi bẩn từ b&ecirc;n ngo&agrave;i.</li>
	<li>Kệ c&oacute; thiết kế th&ecirc;m 1 ngăn k&eacute;o CHỐNG NƯỚC để đồ rất tiện lợi.</li>
</ul>
', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (12, N'Mẫu kệ nhả kem được thiết kế cốc nằm ngang', 3, CAST(260000 AS Decimal(18, 0)), N'<div class="panel entry-content">
				

<p>Bàn chải đánh răng, kem đánh răng sử dụng hàng ngày và đồ dùng cá nhân trên bàn máy tính hoặc bàn trang điểm của bạn thường hay để lung tung không có khay đựng.</p>
<p>Bây giờ bạn có thể thay đổi điều đó để cho căn phòng và bàn làm việc của bạn gọn gàn và tiện dụng hơn bằng sản phẩm của chúng tôi cung cấp.</p>
<h3>Hộp đựng bàn chải, nhả kem đánh răng treo tường</h3>
<p>Treo được 4 cốc. Treo 4 bàn chải đánh răng. Có rãnh đựng dao cạo râu, lược chải tóc. Hộp dễ dàng tháo lắp để vệ sinh. Miếng dán công nghệ mới, siêu chắc và bền theo thời gian(Không lo tự dơi như các miếp hút chân không thông thường). Chất liệu nhựa ABS, PP siêu bền và đẹp</p>
<ul>
<li><strong>Kích thước đóng gói:</strong> 384mm x 113mm x 200mm</li>
<li><strong>Trọng lượng:</strong> 1020g</li>
</ul>
<p>Đây là hàng cao cấp của thương hiệu ECOCO, dành cho thị trường Nhật Bản</p>
<p>Các chi tiết cực kỳ tinh xảo, cẩn thận. Chất nhựa đẹp, là nhựa nano kháng khuẩn, chống bám bẩn.*</p>
<h3>Hướng dẫn lắp đặt</h3>
<ul>
<li>Bước 1: Làm sạch bề mặt tường (vị trí lắp đặt kệ)</li>
<li>Bước 2: Lắp miếng dán vào kệ &amp; loại bỏ màng bảo vệ miếng dán</li>
<li>Bước 3: Dán kệ lên tường (vị trí lắp đặt)</li>
<li>Bước 4: Nhấc kệ ra &amp; dùng tay ép hết không khí trong miếng dán</li>
<li>Bước 5: Cố định miếng dán trong 12 giờ đồng hồ</li>
<li>Bước 6: Gắn kệ vào vị trí cố định và cho các vật dụng vào (Đảm bảo 12 giờ sau khi miếng dán dính vào tường).</li>
</ul>
<h3>Cam kết với khách hàng</h3>
<p>Sản phẩm đúng thông tin mô tả</p>
			</div>', 2, CAST(460000 AS Decimal(18, 0)), NULL, NULL, CAST(160000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 90, NULL, N'<ul>
<li>Màu sắc: Trắng</li>
<li>Thương hiệu: SHUANGQING</li>
<li>Nguồn gốc: Trung Quốc</li>
<li>Kích thước: Đơn: 20.8×15.5×8.8cm – Đôi: 28.7x23x9.2cm</li>
<li>Chất liệu: Nhựa ABS</li>
<li>Trọng lượng: 0.43kg</li>
</ul>', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (13, N'Máy lọc không khí trong nhà AC0820', 2, CAST(3990000 AS Decimal(18, 0)), N'<div class="entry-content panel">
<p>Thư gi&atilde;n đến từng hơi thở với t&iacute;nh năng Chống dị ứng Allergy Care</p>

<p>Chất lượng kh&ocirc;ng kh&iacute; tốt hơn. &Iacute;t l&ocirc;ng th&uacute; cưng hơn</p>

<p>Giảm thiểu m&ugrave;i h&ocirc;i từ th&uacute; cưng. Mang lại bầu kh&ocirc;ng kh&iacute; trong l&agrave;nh tối đa</p>

<p>Bộ lọc quang x&uacute;c t&aacute;c được tăng cường bằng &aacute;nh s&aacute;ng gi&uacute;p loại bỏ th&ecirc;m 94% m&ugrave;i h&ocirc;i từ th&uacute; cưng, đảm bảo ng&ocirc;i nh&agrave; của bạn lu&ocirc;n trong l&agrave;nh v&agrave; sạch sẽ.</p>

<p>Chế độ Pet Mode ph&aacute;t hiện l&ocirc;ng th&uacute; cưng ở c&aacute;c vị tr&iacute; thấp, loại bỏ hơn 30% để ng&ocirc;i nh&agrave; sạch sẽ v&agrave; trong l&agrave;nh hơn. Sản phẩm c&oacute; bộ lọc th&ocirc; dễ d&agrave;ng thay thế.</p>

<p>Thiết kế hiện đại đem lại cảm gi&aacute;c sang trọng</p>

<p>Thiết kế mang đến sự n&acirc;ng cấp cho vẻ đẹp của mọi căn ph&ograve;ng, v&agrave; hệ thống &aacute;nh s&aacute;ng trung t&acirc;m tạo th&ecirc;m kh&ocirc;ng gian, cảm x&uacute;c v&agrave; l&agrave;m dịu bầu kh&ocirc;ng kh&iacute;.</p>
</div>
', NULL, CAST(4990000 AS Decimal(18, 0)), NULL, NULL, CAST(1990000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 80, NULL, N'<p>Lọc kh&ocirc;ng kh&iacute; 360˚</p>

<p>Hệ thống đa bộ lọc</p>

<p>Clean Booster</p>

<p>Chế độ th&uacute; cưng Pet Care</p>

<p>Chống dị ứng Allergy Care</p>

<p>Đ&egrave;n b&aacute;o v&agrave; cảm biến th&ocirc;ng minh</p>
', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (14, N'Bàn Ăn Thông Minh Kéo Dài Mặt Đá', 2, CAST(4550000 AS Decimal(18, 0)), N'<div class="panel entry-content">
<h2>K&iacute;ch thước b&agrave;n ăn k&eacute;o d&agrave;i mặt đ&aacute; nhập khẩu v&agrave; c&aacute;c mẫu b&agrave;n ảnh thực tế</h2>

<p><strong>B&agrave;n Ăn K&eacute;o D&agrave;i</strong>&nbsp;l&agrave; mẫu&nbsp;<strong><a href="https://bachma.vn/ban-an-dep/">b&agrave;n ăn đẹp</a></strong> đang được ưa chuộng hiện nay. Đ&acirc;y l&agrave; d&ograve;ng <strong><a href="https://bachma.vn/ban-an-thong-minh/">b&agrave;n ăn th&ocirc;ng minh</a></strong> nhập khẩu ng&agrave;y c&agrave;ng được ưa chuộng. nhờ khả năng thay đổi diện t&iacute;ch mặt b&agrave;n ăn.cũng như thay đổi k&iacute;ch thước kiểu d&aacute;ng.</p>

<ul>
	<li>Chất liệu: Chất liệu: mặt đ&aacute; ceramic chống xước, chống ẩm,<br />
	chống thấm, ch&acirc;n gỗ đen nh&aacute;m c&oacute; n&uacute;m đồng.</li>
	<li>M&agrave;u sắc: đen, trắng, ghi</li>
	<li>ch&acirc;n b&agrave;n gỗ tự nhi&ecirc;n,</li>
	<li>hệ ray trượt bằng th&eacute;p.</li>
	<li>K&iacute;ch thước khi gấp gọn: rộng 750 mm x cao 750 mm x d&agrave;i 1100 mm</li>
	<li>K&iacute;ch thước khi k&eacute;o d&agrave;i:&nbsp;rộng 750 mm x cao 750 mm x d&agrave;i 1400 mm</li>
	<li>Bảo h&agrave;nh: 12 th&aacute;ng</li>
</ul>

<h2>Ưu điểm b&agrave;n th&ocirc;ng minh k&eacute;o d&agrave;i</h2>

<ul>
	<li>L&agrave; sản phẩm nội thất th&ocirc;ng minh đa năng c&oacute; thể mở rộng v&agrave; thu gọn t&ugrave;y nhu cầu sử dụng</li>
	<li>Gi&uacute;p tiết kiệm v&agrave; tối ưu kh&ocirc;ng gian nhất l&agrave; những căn hộ c&oacute; diện t&iacute;ch nhỏ rất hiệu quả</li>
	<li>Sử dụng dễ d&agrave;ng tiện lợi</li>
	<li>M&agrave;u sắc phong ph&uacute;, c&oacute; thể t&ugrave;y &yacute; điều chỉnh theo nhu cầu của kh&aacute;ch h&agrave;ng</li>
	<li>Sản phẩm c&oacute; mẫu m&atilde; đẹp đa dạng k&iacute;ch thước c&oacute; thể t&ugrave;y chỉnh dựa theo kh&ocirc;ng gian căn hộ</li>
	<li>C&oacute; thể t&ugrave;y chọn vật liệu thiết kế, từ gỗ c&ocirc;ng nghiệp đến gỗ tự nhi&ecirc;n.</li>
	<li>Thời gian bảo h&agrave;nh 12 th&aacute;ng</li>
	<li>Cơ kh&iacute; được bảo h&agrave;nh 12 th&aacute;ng</li>
</ul>

<h2>Cấu tạo b&agrave;n ăn k&eacute;o d&agrave;i</h2>

<p>L&agrave; sự kết hợp của hệ cơ kh&iacute; ray trượt th&ocirc;ng minh. kết hợp với kiểu d&aacute;ng hiện đại v&agrave; sang trọng. Vật liệu l&agrave; sự kết hợp của gỗ tự nhi&ecirc;n. bề mặt gỗ c&ocirc;ng nghiệp chất lượng cao gi&uacute;p sản phẩm bền vững theo thời gian. kh&ocirc;ng lo cong v&ecirc;nh với thời tiết khắc nghiệt của Việt Nam.</p> </div>
', 1, CAST(7500000 AS Decimal(18, 0)), NULL, NULL, CAST(1550000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 40, NULL, N'<p style="text-align:left"><strong>B&agrave;n Ăn Th&ocirc;ng Minh K&eacute;o D&agrave;i</strong> Mặt Đ&aacute; BAKBT01 l&agrave; d&ograve;ng <strong><a href="https://bachma.vn/ban-an-mat-da/">b&agrave;n ăn mặt đ&aacute;</a></strong> th&ocirc;ng minh nhập khẩu nguy&ecirc;n chiếc. thiết kế k&eacute;o d&agrave;i 2 đầu. Với k&iacute;ch thước nhỏ l&agrave; 110 cm, k&iacute;ch thước k&eacute;o d&agrave;i l&agrave; 140cm. Mặt đ&aacute; Ceramic chống chầy xước, chống ố nước c&ocirc;ng nghệ mới nhất hiện nay. Khung th&eacute;p sơn tĩnh điện chống hoen rỉ.</p>
', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (15, N'Bàn Trà Phòng Khách Nhỏ Bằng Gỗ Cho Chung Cư KGTN 011BT002', 2, CAST(3400000 AS Decimal(18, 0)), N'<div class="entry-content panel"> 
<p><span style="color:#0000ff"><a href="https://khonggiantiennghi.vn/ban-tra-phong-khach-nho-bang-go-cho-chung-cu-kgtn-011bt002/" style="color: #0000ff;"><strong>B&agrave;n tr&agrave; ph&ograve;ng kh&aacute;ch nhỏ bằng gỗ cho chung cư KGTN 011BT002</strong></a></span> c&oacute; thiết kế phong c&aacute;ch Bắc &Acirc;u gồm 2 b&agrave;n ri&ecirc;ng lẻ kết hợp : h&igrave;nh trứng v&agrave; h&igrave;nh tr&ograve;n. Sản phẩm l&agrave;m từ gỗ kết hợp sắt sơn tĩnh điện kh&ocirc;ng gỉ s&eacute;t ph&ugrave; hợp cho nhiều kh&ocirc;ng gian nh&agrave; ở v&agrave; chung cư hiện đại.</p>

<p><strong><span style="color:#0000ff"><a href="https://khonggiantiennghi.vn/ban-tra/" style="color: #0000ff;">B&agrave;n tr&agrave; ph&ograve;ng kh&aacute;ch</a></span> phong c&aacute;ch Bắc &Acirc;u&nbsp;KGTN 011BT002&nbsp;</strong> được l&agrave;m từ gỗ c&ocirc;ng nghiệp c&oacute; mặt v&acirc;n gỗ chấm thấm nước, dễ d&agrave;ng lau ch&ugrave;i, ch&acirc;n l&agrave;m từ sắt sơn tĩnh điện kh&ocirc;ng gỉ, chịu lực tốt.</p>

<p><strong>B&agrave;n tr&agrave; gỗ c&oacute; ch&acirc;n sắt KGTN 011BT002</strong> c&oacute; thiết kế h&igrave;nh trứng v&agrave; h&igrave;nh tr&ograve;n kh&ocirc;ng tạo g&oacute;c nhọn đảm bảo an to&agrave;n cho người sử dụng, đặc biệt l&agrave; c&aacute;c gia đ&igrave;nh c&oacute; trẻ nhỏ.</p>
</div>
', 1, CAST(4400000 AS Decimal(18, 0)), NULL, N'', CAST(1400000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 60, NULL, N'<p>B&agrave;n tr&agrave; ph&ograve;ng kh&aacute;ch nhỏ bằng gỗ cho chung cư KGTN 011BT002 c&oacute; thiết kế phong c&aacute;ch Bắc &Acirc;u gồm 2 b&agrave;n ri&ecirc;ng lẻ kết hợp : h&igrave;nh trứng v&agrave; h&igrave;nh tr&ograve;n l&agrave;m từ gỗ kết hợp sắt sơn tĩnh điện kh&ocirc;ng gỉ s&eacute;t. C&oacute; vận chuyển to&agrave;n quốc</p>
', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (16, N'Đồng Hồ Treo Tường Trang Trí Nghệ Thuật KGTN 011DH001', 2, CAST(1150000 AS Decimal(18, 0)), N'<p><span style="color:#0000ff"><a href="https://khonggiantiennghi.vn/dong-ho-treo-tuong-trang-tri-nghe-thuat-kgtn-011dh001/" style="color: #0000ff;"><strong>Đồng hồ treo tường trang tr&iacute; nghệ thuật KGTN 011DH001</strong></a></span> sở hữu đa m&agrave;u sắc, mang lại cảm gi&aacute;c tươi s&aacute;ng cho kh&ocirc;ng gian. Sản phẩm l&agrave;m từ gỗ kết hợp kim loại sơn m&agrave;u. Dễ d&agrave;ng lắp đặt.</p>

<p><strong>KGTN 011DH001</strong> c&oacute; thiết kế h&igrave;nh tr&ograve;n phong c&aacute;ch tối giản. C&aacute;c mốc thời gian được biểu thị bằng những tr&aacute;i b&oacute;ng gỗ sắc m&agrave;u đẹp mắt. Sản phẩm c&oacute; t&iacute;nh thẩm mỹ nghệ thuật cao, ph&ugrave; hợp trang tr&iacute; cho nhiều kh&ocirc;ng gian ph&ograve;ng ở.</p>

<p>Động cơ đồng hồ kim tr&ocirc;i. Sử dụng pin AA. Dễ d&agrave;ng t&igrave;m mua ở ngo&agrave;i thị trường để thay&nbsp; khi cần.</p>
', 2, CAST(2150000 AS Decimal(18, 0)), NULL, N'', CAST(4150000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 5, NULL, N'<p>Đồng hồ treo tường trang tr&iacute; nghệ thuật KGTN 011DH001 sở hữu đa m&agrave;u sắc, mang lại cảm gi&aacute;c tươi s&aacute;ng cho kh&ocirc;ng gian. Sản phẩm l&agrave;m từ gỗ kết hợp kim loại sơn m&agrave;u. Dễ d&agrave;ng lắp đặt.</p>
', 2)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (17, N'Giá Kệ Nhà Tắm Bằng Nhựa 3 Tầng Kgtn 011KNT008', 3, CAST(650000 AS Decimal(18, 0)), N'<p><span style="color: #0000ff;"><a style="color: #0000ff;" href="https://khonggiantiennghi.vn/gia-ke-nha-tam-bang-nhua-3-tang-kgtn-011knt008/"><strong>Giá kệ nhà tắm bằng nhựa 3 tầng kgtn 011KNT008</strong></a></span> thiết kế thông minh, tận dụng không gian trên bồn cầu làm nơi lưu trữ. Dễ dàng lắp đặt và sử dụng. Chất liệu chống nước và không han gỉ.</p>
<p><strong>Kệ nhà tắm bằng nhựa 3 tầng KGTN 011KNT008</strong> thiết kế dạng lắp ráp, là giải pháp hữu ích cho người dùng cất trữ những đồ nhỏ gọn như: chai lọ sữa tắm, dầu gội, để khăn .v.v.</p>
<p>Sản phẩm có thành cao giúp ngừa việc rơi đồ vật. Đế có lỗ giúp thoát nước nhanh, sạch sẽ, dễ vệ sinh.</p>
<p><strong>Kệ phòng tắm bằng nhựa KGTN 011KNT008</strong> dễ dàng lắp ráp và lắp đặt tại nhà bằng những thao tác đơn giản.</p>
<p><strong>Kệ nhựa nhà tắm 3 tầng KGTN 011KNT008</strong> được bán tại <strong>Không Gian Tiện Nghi</strong> với 2 màu trắng và tím. Có giao hàng toàn quốc.</p>', 1, CAST(750000 AS Decimal(18, 0)), NULL, N'', CAST(250000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 70, NULL, N'<p>Giá kệ nhà tắm bằng nhựa 3 tầng kgtn 011KNT008 thiết kế thông minh, tận dụng không gian trên bồn cầu làm nơi lưu trữ, dễ dàng lắp đặt và sử dụng. Chất liệu chống nước và không han gỉ.</p>', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (18, N'Kệ để đồ nhà kho', 5, CAST(200000 AS Decimal(18, 0)), N'<p>Tiện dụng treo đồ</p>
', 1, CAST(180000 AS Decimal(18, 0)), NULL, N'', CAST(150000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 10, NULL, N'<p>Tiện dụng treo đồ</p>
', -14)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (19, N'Xẻng trồng cây đa năng', 5, CAST(200000 AS Decimal(18, 0)), N'<p>TH&Ocirc;NG TIN SẢN PHẨM<br />
- Xẻng trồng c&acirc;y inox được l&agrave;m từ inox chống gỉ<br />
- C&aacute;n được l&agrave;m từ cao su n&ecirc;n khi sử dụng rất &ecirc;m tay, t&aacute;c động mạnh kh&ocirc;ng sợ bị phồng tay<br />
- Xẻng inox cứng cắp.<br />
- D&ugrave;ng để xới, x&uacute;c khi trồng c&acirc;y, trồng rau, hoa c&aacute;c loại<br />
K&iacute;ch thước. to&agrave;n bộ xẻng D&agrave;i 32cm, từ mũi xẻng tới c&aacute;n d&agrave;i 19cm, măt rộng nhất của xẻng rộng 9cm.</p>

<p><input alt="" src="https://www.google.com.vn/url?sa=i&amp;url=https%3A%2F%2Fubl.vn%2Fproducts%2Fxeng-lam-vuon&amp;psig=AOvVaw3Moj_E7KvOvbJ068lyaI35&amp;ust=1716483846357000&amp;source=images&amp;cd=vfe&amp;opi=89978449&amp;ved=0CBIQjRxqFwoTCPikl-_eoYYDFQAAAAAdAAAAABAE" type="image" /><input alt="" src="https://product.hstatic.net/1000058213/product/ot0501__1__master.jpg" style="float:left; height:100px; width:100px" type="image" /><input alt="" src="https://bizweb.dktcdn.net/100/355/080/products/bonsai-512a-1.jpg?v=1667242360203" style="float:left; height:100px; width:100px" type="image" /><input alt="" src="https://levu.vn/wp-content/uploads/2022/10/xeng-xuc-cat-chua-chay-gia-re.jpg" style="float:left; height:100px; width:91px" type="image" /></p>
', 1, CAST(180000 AS Decimal(18, 0)), NULL, NULL, CAST(150000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 40, NULL, N'<h1><span style="font-size:14px"><img alt="" src="https://bizweb.dktcdn.net/thumb/large/100/351/215/products/tolsen-58001-2-jpeg-403eb358-d0e4-4d26-95a8-1b1c785f4427.jpg?v=1689176707777" style="float:left; height:100px; width:100px" />Xẻng inox trồng c&acirc;y, xẻng trồng c&acirc;y chuy&ecirc;n dụng, xẻng mini, xẻng x&uacute;c đất.&nbsp;</span></h1>
', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (20, N'Kéo cắt tỉa cây đa năng', 5, CAST(200000 AS Decimal(18, 0)), N'<p>Để c&oacute; những chậu c&acirc;y đẹp, được tạo d&aacute;ng v&agrave; cắt tỉa gọn g&agrave;ng th&igrave; dụng cụ kh&ocirc;ng thể thiếu m&agrave; c&aacute;c bạn cần trang bị trong bộ dụng cụ l&agrave;m vườn của m&igrave;nh đ&oacute; l&agrave; k&eacute;o cắt tỉa c&acirc;y cảnh. N&oacute; l&agrave; một trợ thủ đắc lực gi&uacute;p bạn tạo cho chậu c&acirc;y bon sai của m&igrave;nh những h&igrave;nh d&aacute;ng thật t&acirc;m đắc.</p>

<p><strong>K&eacute;o cắt tỉa c&acirc;y c&aacute;n tăng cao cấp</strong>&nbsp;d&ugrave;ng để cắt h&agrave;ng r&agrave;o, cắt cỏ, cắt hoa, cắt c&agrave;nh nhỏ,... K&eacute;o được l&agrave;m bằng th&eacute;p mangan chất lượng cao, độ ch&iacute;nh x&aacute;c cao.&nbsp;Lưỡi k&eacute;o sắc b&eacute;n, vết cắt&nbsp;ngọt.&nbsp;C&aacute;n k&eacute;o được chế tạo bằng hợp kim nh&ocirc;m, nhẹ, v&agrave; c&oacute; độ cứng tốt. Một phần c&aacute;n k&eacute;o được bọc nhựa mềm, gi&uacute;p chống trơn trượt khi tay c&oacute; nước. D&ugrave;ng để cắt tỉa cỏ, c&acirc;y cảnh, ph&aacute;t quang bụi rậm, c&acirc;y xanh c&ocirc;ng vi&ecirc;n đ&ocirc; thị... v&ocirc; c&ugrave;ng tiện lợi</p>

<p>&nbsp;</p>

<h2><span style="color:#2ecc71"><strong>Th&ocirc;ng số kỹ thuật&nbsp;K&eacute;o cắt tỉa c&acirc;y c&aacute;n tăng cao cấp</strong></span></h2>

<ul>
	<li>K&iacute;ch thước: 445mm</li>
	<li>Đường k&iacute;nh cắt 2cm</li>
	<li>Lưỡi d&agrave;i: 270mm</li>
	<li>M&agrave;u sắc:&nbsp;C&aacute;n v&agrave;ng, tay cầm đen</li>
	<li>Chất liệu lưỡi:&nbsp;th&eacute;p kh&ocirc;ng gỉ</li>
	<li>C&aacute;n cầm c&oacute; thể điểu chỉnh k&iacute;ch thước</li>
	<li>Thuận tiện khi sử dụng</li>
	<li>Sử dụng hiệu quả để tạo t&aacute;n h&agrave;ng r&agrave;o c&acirc;y cảnh</li>
</ul>

<p>&nbsp;</p>

<h2><span style="color:#2ecc71"><strong>Đặc điểm nổi bật&nbsp;K&eacute;o cắt tỉa c&acirc;y c&aacute;n tăng cao cấp</strong></span></h2>

<ul>
	<li>Lưỡi k&eacute;o rất sắc v&agrave; nhọn, được thiết kế đặc biệt để ph&ugrave; hợp với những c&ocirc;ng việc l&agrave;m vườn. Đ&aacute;p ứng rất tốt cho nhu cầu sử dụng trong l&agrave;m vườn n&oacute;i chung v&agrave; cắt tỉa h&agrave;ng r&agrave;o n&oacute;i ri&ecirc;ng.</li>
	<li>Lưỡi k&eacute;o được l&agrave;m bằng th&eacute;p kh&ocirc;ng gỉ, bản k&eacute;o d&agrave;y rất chắc chắn tạo lực cắt rất khỏe. Cắt tỉa h&agrave;ng r&agrave;o v&agrave; cắt cỏ rất nhanh</li>
	<li>Tay cầm của&nbsp;k&eacute;o cắt tỉa h&agrave;ng r&agrave;o&nbsp;được bọc bằng nhựa dẻo n&ecirc;n rất &ecirc;m &aacute;i c&oacute; t&aacute;c dụng gi&uacute;p thời gian thao t&aacute;c của bạn được k&eacute;o d&agrave;i. Kh&ocirc;ng chỉ vậy giới hạn dịch chuyển tay cầm cũng được l&agrave;m bằng hai gối đỡ cao su &ecirc;m &aacute;i n&ecirc;n tạo cảm gi&aacute;c thoải m&aacute;i cho người sử dụng. Điều đặc biệt l&agrave; tay cầm c&oacute; thể k&eacute;o d&agrave;i gấp đ&ocirc;i bằng hệ thống ống lối c&oacute; r&atilde;nh xắn b&ecirc;n trong rất thuận tiện gi&uacute;p tầm cắt xa v&agrave; cao hơn.</li>
	<li>Điều đặc biệt l&agrave; tay cầm c&oacute; thể k&eacute;o d&agrave;i gấp đ&ocirc;i bằng hệ thống ống lối c&oacute; r&atilde;nh xắn b&ecirc;n trong rất thuận tiện gi&uacute;p tầm cắt xa v&agrave; cao hơn.</li>
</ul>
', 2, CAST(180000 AS Decimal(18, 0)), NULL, NULL, CAST(170000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 50, NULL, N'<ul>
	<li>Lưỡi k&eacute;o sắc b&eacute;n, vết cắt&nbsp;ngọt</li>
	<li>Lưỡi bằng th&eacute;p độ bền cao</li>
	<li>C&aacute;n c&oacute; tăng c&oacute; thể thay đổi chiều d&agrave;i k&eacute;o</li>
	<li>Tay cầm bọc cao su, chống trơn trượt khi sử dụng</li>
	<li>K&eacute;o dễ d&agrave;ng điều chỉnh cho ph&ugrave; hợp tư thế người sử dụng</li>
</ul>
', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (21, N'Bếp Nướng Điện Không Khói 3 Khay BEAR Chính Hãng [HOT], Nồi Lẩu Nướng - Bếp Lẩu Nướng 3 In 1 Loaị Nồi Lẩu Điện Đa Năng', 1, CAST(1300000 AS Decimal(18, 0)), N'<h2>Giới thiệu Bếp Nướng Điện Kh&ocirc;ng Kh&oacute;i 3 Khay BEAR Ch&iacute;nh H&atilde;ng [HOT], Nồi Lẩu Nướng - Bếp Lẩu Nướng 3 In 1 Loaị Nồi Lẩu Điện Đa Năng</h2>

<p>Bếp Nướng Điện Kh&ocirc;ng Kh&oacute;i 3 Khay Bear Ch&iacute;nh H&atilde;ng, Nồi Lẩu Điện Đa Năng, Nồi Lẩu Nướng, Bếp Lẩu Nướng 3 In 1 Bear Bản Quốc Tế<br />
✅ Cam Kết Tại Gia Dụng Đại Ph&aacute;t<br />
- Tổng kho đồ gia dụng Đại Ph&aacute;t l&agrave; tổng kho lớn uy t&iacute;n tại miền Bắc, uy t&iacute;n được đảm bảo qua h&agrave;ng vạn đơn h&agrave;ng tr&ecirc;n khắp cả nước<br />
- Bếp Nướng Điện, Nồi Lẩu Điện, Bếp Nướng Điện, Nồi Lẩu 2 Ngăn, Nồi Lẩu Nướng Bear 100% giống m&ocirc; tả<br />
- Đảm bảo chất lượng, dịch vụ tốt nhất, h&agrave;ng được giao từ 1-3 ng&agrave;y kể từ ng&agrave;y đặt h&agrave;ng<br />
- Giao h&agrave;ng to&agrave;n quốc theo h&igrave;nh thức COD hoặc qua v&iacute; Airpay<br />
- Qu&yacute; kh&aacute;ch h&agrave;ng add m&atilde; Freeship để được hưởng ch&iacute;nh s&aacute;ch miễn ph&iacute; vận chuyển từ Shopee<br />
- Bảo h&agrave;nh ch&iacute;nh h&atilde;ng 12 th&aacute;ng, đổi trả trong v&ograve;ng 7 ng&agrave;y nếu do lỗi nh&agrave; sản xuất<br />
- Địa chỉ: 342 Khương Đ&igrave;nh- Thanh Xu&acirc;n - H&agrave; Nội<br />
- Hotline: 0968980462<br />
- Fanpage: https://www.facebook.com/thegioidogiadungvaphukien<br />
✅TH&Ocirc;NG TIN SẢN PHẨM<br />
- T&ecirc;n sản phẩm: Bếp Nướng Điện, Nồi Lẩu Điện, nồi lẩu nướng, nồi lẩu đa năng, bếp lẩu nướng Bear<br />
- Điện &aacute;p: 220V ~ 50Hz<br />
- C&ocirc;ng suất: 1200W<br />
- Nhiệt độ: 0 - 230 độ C<br />
- Độ s&acirc;u nồi lẩu: 60mm<br />
- Dung t&iacute;ch nồi lẩu: 4L<br />
- Độ s&acirc;u vỉ nướng: 35mm<br />
- Chất liệu bộ ph&aacute;t nhiệt:Inox 304<br />
- Phương thức điều khiển: N&uacute;t vặn<br />
- Chất liệu: Vỏ chống d&iacute;nh an to&agrave;n<br />
✔️ T&iacute;ch hợp 3 khay th&aacute;o rời: Khay lẩu, khay nướng v&agrave; khay l&agrave;m b&aacute;nh<br />
✔️ C&ocirc;ng suất l&ecirc;n tới 1200W<br />
✔️ N&uacute;t điều chỉnh nhiệt tiện lợi<br />
✔️ Lớp phủ chống d&iacute;nh đặc biệt<br />
✔️ Thiết kế khay rời, dễ d&agrave;ng vệ sinh, an to&agrave;n<br />
✔️ M&agrave;u xanh ngọc b&iacute;ch trang nh&atilde; thanh lịch<br />
- H&agrave;ng quốc tế<br />
- 1 đổi 1 trong 30 ng&agrave;y nếu c&oacute; lỗi kỹ thuật từ nh&agrave; sản xuất.<br />
- Bảo h&agrave;nh 12 th&aacute;ng</p>
', 2, CAST(1250000 AS Decimal(18, 0)), NULL, NULL, CAST(1200000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 5, NULL, N'<p>✔️ Nồi lẩu nướng đa năng Bear SUBE004 (DHG-C40D5) bản quốc tế với bảng điều khiển tiếng anh, chăn cắm 2 chấu tiện lợi đi k&egrave;m hướng dẫn sử dụng tiếng Việt tiện lợi v&agrave; dễ sử dụng. Đặc biệt, nồi lẩu 3 khay Bear SUBE004 sở hữu thiết kế th&ocirc;ng minh, t&iacute;ch hợp 3 chức năng trong 1 si&ecirc;u tiện lợi cho ph&eacute;p bạn l&agrave;m nhiều m&oacute;n ăn kh&aacute;c nhau, thay đổi nhu cầu sửu dụng nhanh ch&oacute;ng.</p>
', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (22, N'Đèn Ngủ Để Bàn Hiện Đại Chao Vải Trang Trí Phòng Ngủ Đẹp AN-B6856', NULL, CAST(180000 AS Decimal(18, 0)), N'<p>B&oacute;ng đ&egrave;n: B&oacute;ng led đui xo&aacute;y E27 s&aacute;ng v&agrave;ng, c&ocirc;ng suất 3w-7w</p>

<p>Ch&acirc;n đ&egrave;n: Đế đồng/ đế gỗ hoặc đế 3 ch&acirc;n</p>

<p>Chao đ&egrave;n: Vải lụa cao cấp</p>

<p><strong>Đ&egrave;n ngủ để b&agrave;n</strong>&nbsp;hoặc để t&aacute;p đầu giường thuận tiện dễ d&agrave;ng tắt/bật</p>

<p>&Aacute;nh s&aacute;ng v&agrave;ng dịu nhẹ dễ ngủ, cũng c&oacute; thể đọc s&aacute;ch.</p>

<p>H&agrave; Nội led giới thiệu 11 mẫu&nbsp;<a href="https://hanoiled.com/san-pham/den-ngu-de-ban/">đ&egrave;n ngủ để b&agrave;n</a></p>
', 1, CAST(160000 AS Decimal(18, 0)), NULL, NULL, CAST(135000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 5, NULL, N'<ul>
	<li>
	<p><strong>K&iacute;ch thước :</strong>&nbsp;W235&nbsp;x H420mm</p>
	</li>
	<li>
	<p><strong>B&oacute;ng đ&egrave;n :</strong>&nbsp;E27 x 1 (chưa bao gồm)</p>
	</li>
	<li>
	<p><strong>Chất liệu :</strong></p>
	</li>
	<li>
	<p>- Th&acirc;n đ&egrave;n bằng hợp kim</p>
	</li>
	<li>
	<p>- Chao đ&egrave;n bằng vải</p>
	</li>
	<li>
	<p><strong>Xuất xứ :</strong>&nbsp;nhập khẩu</p>
	</li>
	<li>
	<p><strong>Bảo h&agrave;nh :</strong>&nbsp;12 th&aacute;ng</p>
	</li>
</ul>
', NULL)
INSERT [dbo].[products] ([id], [name], [category_id], [price_sell], [description], [status], [price_reduced], [horizontal], [trademark], [price_import], [trademarkId], [ishidden], [sizes], [images], [number_import], [cover_type], [shortdescription], [total_sell]) VALUES (25, N'Đèn Ngủ Để Bàn Hiện Đại ', 8, CAST(300000 AS Decimal(18, 0)), N'<p>dễ d&agrave;ng sử dụng</p>
', 1, CAST(280000 AS Decimal(18, 0)), NULL, NULL, CAST(250000 AS Decimal(18, 0)), NULL, 0, NULL, NULL, 50, NULL, N'<p>đ&egrave;n ngủ tốt</p>
', NULL)
SET IDENTITY_INSERT [dbo].[products] OFF
GO
SET IDENTITY_INSERT [dbo].[reviews] ON 

INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (1, 6, CAST(N'2024-04-22T14:17:00.1460609' AS DateTime2), N'2222', NULL, 4, N'Lê Xuân Hải')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (2, 10, CAST(N'2024-04-22T14:23:11.9750211' AS DateTime2), N'444', NULL, 3, N'Lê Xuân Hải')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (3, 7, CAST(N'2024-05-09T22:49:20.6205023' AS DateTime2), N'Máy sấy tiện dụng', NULL, 5, N'Bùi Thị Thơm')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (4, 15, CAST(N'2024-05-10T21:47:04.3918503' AS DateTime2), N'Sản phẩm tốt', NULL, 4, N'Nguyễn Thị Hoài Thu')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (5, 7, CAST(N'2024-05-11T16:30:46.1682590' AS DateTime2), N'Máy sấy tiện dụng', NULL, 5, N'Nguyễn Thị Hoài Thu')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (6, 3, CAST(N'2024-05-11T16:31:38.9800751' AS DateTime2), N'Sử dụng tốt', NULL, 3, N'Nguyễn Thị Hoài Thu')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (7, 7, CAST(N'2024-05-11T23:06:29.3392120' AS DateTime2), N'Sử dụng tốt', NULL, 5, N'Nguyễn Thị Hoài Thu')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (8, 4, CAST(N'2024-05-11T23:35:30.2360433' AS DateTime2), N'Sử dụng tốt', NULL, 3, N'Nguyễn Thị Hoài Thu')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (9, 5, CAST(N'2024-05-15T21:22:52.6489972' AS DateTime2), N'Sử dụng tốt', NULL, 3, N'Nguyễn Thị Hoài Thu')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (10, 7, CAST(N'2024-05-17T15:42:55.1333592' AS DateTime2), N'Sử dụng tốt', NULL, 4, N'Bùi Thị Thơm')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (11, 7, CAST(N'2024-05-17T15:43:30.1388770' AS DateTime2), N'Sản phẩm sử dụng bình thường', NULL, 3, N'Nguyễn Thị Hoài Thu')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (12, 14, CAST(N'2024-05-17T15:44:13.4273435' AS DateTime2), N'Sản phẩm sử dụng rất tiện dụng trong gia đình', NULL, 4, N'Nguyễn Thị Hoài Thu')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (13, 11, CAST(N'2024-05-19T02:04:54.7195351' AS DateTime2), N'Sản phẩm sử dụng rất tiện dụng trong gia đình', NULL, 5, N'Bùi Thị Thơm')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (14, 18, CAST(N'2024-05-27T13:10:30.9714842' AS DateTime2), N'Sản phẩm sử dụng rất tiện dụng trong gia đình', NULL, 4, N'Bùi Thị Thơm')
INSERT [dbo].[reviews] ([id], [product_id], [created_at], [comment], [status], [star], [fullname]) VALUES (15, 6, CAST(N'2024-05-27T13:45:11.1582107' AS DateTime2), N'Sản phẩm sử dụng rất tiện dụng trong gia đình', NULL, 4, N'Bùi Thị Thơm')
SET IDENTITY_INSERT [dbo].[reviews] OFF
GO
SET IDENTITY_INSERT [dbo].[users] ON 

INSERT [dbo].[users] ([id], [full_name], [phone_number], [email], [avatar], [code], [address], [role], [password], [status], [register_date]) VALUES (1, N'Bùi Thị Thơm', N'0345801987', N'buithom222@gmail.com', NULL, N'', N'Nghệ An', 0, N'123456789', 0, NULL)
INSERT [dbo].[users] ([id], [full_name], [phone_number], [email], [avatar], [code], [address], [role], [password], [status], [register_date]) VALUES (3, N'Nguyễn Thị Hương', N'0345801876', N'nguyenthihuong@gmail.com', NULL, NULL, NULL, 0, N'12345678', 0, NULL)
INSERT [dbo].[users] ([id], [full_name], [phone_number], [email], [avatar], [code], [address], [role], [password], [status], [register_date]) VALUES (4, N'Nguyễn Văn Hùng', N'0345801872', N'nguyenvanhung@gmail.com', NULL, NULL, NULL, 1, N'12345678', 0, NULL)
INSERT [dbo].[users] ([id], [full_name], [phone_number], [email], [avatar], [code], [address], [role], [password], [status], [register_date]) VALUES (5, N'Lê Hồng Thắm', N'0345801213', N'lehongtham@gmail.com', NULL, NULL, NULL, 1, N'12345678', 0, NULL)
INSERT [dbo].[users] ([id], [full_name], [phone_number], [email], [avatar], [code], [address], [role], [password], [status], [register_date]) VALUES (6, N'Bùi Thị Thơm', N'0978659803', N'buithom747@gmail.com', N'bóngchuy?n.jpg', N'', N'Yên Thành, Nghệ An', 1, N'123456789', 0, CAST(N'2024-05-08T22:36:07.9105974' AS DateTime2))
INSERT [dbo].[users] ([id], [full_name], [phone_number], [email], [avatar], [code], [address], [role], [password], [status], [register_date]) VALUES (8, N'Trần Văn Nhất', N'0936411789', N'nhatnhat66@gmail.com', NULL, NULL, N'Hà Nội', 1, N'12345678', 0, CAST(N'2024-05-17T02:09:58.3212671' AS DateTime2))
INSERT [dbo].[users] ([id], [full_name], [phone_number], [email], [avatar], [code], [address], [role], [password], [status], [register_date]) VALUES (9, N'Châu Thi Vũ', N'0978659888', N'chauthivu123@gmail.com', NULL, NULL, N'Nghệ An', 1, N'12345678', 0, CAST(N'2024-05-17T02:12:09.6830954' AS DateTime2))
INSERT [dbo].[users] ([id], [full_name], [phone_number], [email], [avatar], [code], [address], [role], [password], [status], [register_date]) VALUES (11, N'Nguyễn Thị Nga', N'0345801234', N'nguyennga123@gmail.com', NULL, NULL, NULL, 1, N'123456789', 0, CAST(N'2024-05-27T13:42:09.5066845' AS DateTime2))
SET IDENTITY_INSERT [dbo].[users] OFF
GO
ALTER TABLE [dbo].[carts]  WITH CHECK ADD  CONSTRAINT [FK_carts_users] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
ALTER TABLE [dbo].[carts] CHECK CONSTRAINT [FK_carts_users]
GO
ALTER TABLE [dbo].[images]  WITH CHECK ADD  CONSTRAINT [FK_images_products] FOREIGN KEY([product_id])
REFERENCES [dbo].[products] ([id])
GO
ALTER TABLE [dbo].[images] CHECK CONSTRAINT [FK_images_products]
GO
ALTER TABLE [dbo].[order_details]  WITH CHECK ADD  CONSTRAINT [FK_order_details_orders_order_id] FOREIGN KEY([order_id])
REFERENCES [dbo].[orders] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[order_details] CHECK CONSTRAINT [FK_order_details_orders_order_id]
GO
ALTER TABLE [dbo].[order_details]  WITH CHECK ADD  CONSTRAINT [FK_order_details_products_product_id] FOREIGN KEY([product_id])
REFERENCES [dbo].[products] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[order_details] CHECK CONSTRAINT [FK_order_details_products_product_id]
GO
ALTER TABLE [dbo].[orders]  WITH CHECK ADD  CONSTRAINT [FK_orders_users_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[orders] CHECK CONSTRAINT [FK_orders_users_user_id]
GO
ALTER TABLE [dbo].[posts]  WITH CHECK ADD  CONSTRAINT [FK_posts_users] FOREIGN KEY([author])
REFERENCES [dbo].[users] ([id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[posts] CHECK CONSTRAINT [FK_posts_users]
GO
ALTER TABLE [dbo].[ProductCart]  WITH CHECK ADD  CONSTRAINT [FK_ProductCart_carts] FOREIGN KEY([CartId])
REFERENCES [dbo].[carts] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductCart] CHECK CONSTRAINT [FK_ProductCart_carts]
GO
ALTER TABLE [dbo].[ProductCart]  WITH CHECK ADD  CONSTRAINT [FK_ProductCart_products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[products] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductCart] CHECK CONSTRAINT [FK_ProductCart_products]
GO
ALTER TABLE [dbo].[products]  WITH CHECK ADD  CONSTRAINT [FK_products_categories_category_id] FOREIGN KEY([category_id])
REFERENCES [dbo].[categories] ([id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[products] CHECK CONSTRAINT [FK_products_categories_category_id]
GO
ALTER TABLE [dbo].[reviews]  WITH CHECK ADD  CONSTRAINT [FK_reviews_products_product_id] FOREIGN KEY([product_id])
REFERENCES [dbo].[products] ([id])
GO
ALTER TABLE [dbo].[reviews] CHECK CONSTRAINT [FK_reviews_products_product_id]
GO
